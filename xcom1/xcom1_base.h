/*----------------------------------------------------------------------------*/
#ifndef __MY1XCOM1_BASE_H__
#define __MY1XCOM1_BASE_H__
/*----------------------------------------------------------------------------*/
#include "my1type.h"
/*----------------------------------------------------------------------------*/
#define BASE_NAME_SIZE 14
#define BASE_TILE_COUNT 36
/*----------------------------------------------------------------------------*/
typedef struct __base_t {
	abyte name[BASE_NAME_SIZE];
	aword name_sep; /** null */
	aword detection_short, detection_long, detection_hyperwave;
	abyte build_type[BASE_TILE_COUNT]; /** offset: 0x16 */
	abyte build_to_finish[BASE_TILE_COUNT];
	abyte count_engineer, count_scientist;
	aword item_stingray_launcher; /* item00 */
	aword item_avalanche_launcher;
	aword item_canon;
	aword item_fusion_ball_launcher;
	aword item_laser_canon;
	aword item_plasma_beam; /** offset: 0x6a */
	aword item_stingray_missile;
	aword item_avalanche_missile;
	aword item_canon_rounds_x50;
	aword item_fusion_ball;
	aword tank_cannon; /* item10 */
	aword tank_rocket_launcher;
	aword tank_laser_canon;
	aword hovertank_plasma;
	aword hovertank_launcher;
	aword item_pistol;
	aword item_pistol_clip;
	aword item_rifle;
	aword item_rifle_clip;
	aword item_heavy_canon;
	aword item_hc_ap_ammo; /* item20 */
	aword item_hc_he_ammo;
	aword item_hc_i_ammo;
	aword item_auto_canon;
	aword item_ac_ap_ammo;
	aword item_ac_he_ammo;
	aword item_ac_i_ammo;
	aword item_rocket_launcher;
	aword item_small_rocket;
	aword item_large_rocket;
	aword item_incendiary_rocket; /* item30 */
	aword item_laser_pistol;
	aword item_laser_rifle;
	aword item_heavy_laser;
	aword item_grenade;
	aword item_smoke_grenade;
	aword item_proximity_grenade;
	aword item_high_explosive;
	aword item_motion_scanner;
	aword item_medi_kit;
	aword item_psi_amp; /* item40 */
	aword item_stun_rod;
	aword item_electro_flare;
	aword empty00[3];
	aword corpses[3]; /* only,w/armor,w/powersuit */
	aword item_heavy_plasma;
	aword item_heavy_plasma_clip; /* item50 */
	aword item_plasma_rifle;
	aword item_plasma_rifle_clip;
	aword item_plasma_pistol;
	aword item_plasma_pistol_clip;
	aword item_blaster_launcher;
	aword item_blaster_bomb;
	aword item_small_launcher;
	aword item_stun_bomb;
 	aword item_alien_grenade;
	aword elerium; /** offset: 0xd8 */ /* item60 */
	aword item_mind_probe;
	aword empty01[3]; /* first undefined? */
	aword sectoid_corpse;
	aword snakeman_corpse;
	aword ethereal_corpse;
	aword muton_corpse;
	aword floater_corpse;
	aword celatid_corpse;
	aword silacoid_corpse;
	aword chryssalid_corpse;
	aword reaper_corpse;
	aword sectopod_corpse;
	aword cyberdisc_corpse;
	aword hovertank_corpse;
	aword tank_corpse;
	aword maleciv_corpse;
	aword femaleciv_corpse;
	aword ufo_power_source;
	aword ufo_navigation;
	aword ufo_construction;
	aword alien_food;
	aword alien_reproduction;
	aword alien_entertainment;
	aword alien_surgery;
	aword alien_examination_room;
	aword alien_alloys;
	aword alien_habitat;
	aword item_personal_armor;
	aword item_power_suit;
	aword item_flying_suit;
	aword item_hwp_canon_shells;
	aword item_hwp_rockets;
	aword item_hwp_fusion_bomb;
	adwrd empty_flag; /** 0x0001 if empty @ inactive */
} base_t;  /** size: 0x124 */
/*----------------------------------------------------------------------------*/
#define BASE_INFO_SIZE 292
#define BASE_FULLCOUNT 8
#define BASE_FULLBYTES (BASE_INFO_SIZE*BASE_FULLCOUNT)
#define BASE_DATA_NAME "BASE.DAT"
#define BASE_DATA_NAMESZ 8
/*----------------------------------------------------------------------------*/
#define BASE_TILE_ENTRANCE 0x00
#define BASE_TILE_LIVING_QUARTERS 0x01
#define BASE_TILE_LABORATORY 0x02
#define BASE_TILE_WORKSHOP 0x03
#define BASE_TILE_SMALL_RADAR 0x04
#define BASE_TILE_LARGE_RADAR 0x05
#define BASE_TILE_GENERAL_STORES 0x07
#define BASE_TILE_ALIEN_CONTAINMENT 0x08
#define BASE_TILE_MISSILE_DEFENSE 0x06
#define BASE_TILE_LASER_DEFENSE 0x09
#define BASE_TILE_PLASMA_DEFENSE 0x0A
#define BASE_TILE_FUSION_BALL_DEFENSE 0x0B
#define BASE_TILE_GRAVITY_SHIELD 0x0C
#define BASE_TILE_MIND_SHIELD 0x0D
#define BASE_TILE_PSIONIC_LAB 0x0E
#define BASE_TILE_HYPERWAVE_TRANSLATION 0x0F
#define BASE_TILE_HANGAR_TL 0x10
#define BASE_TILE_HANGAR_TR 0x11
#define BASE_TILE_HANGAR_BL 0x12
#define BASE_TILE_HANGAR_BR 0x13
/*----------------------------------------------------------------------------*/
#define BASE_BUILD0_PLASMABEAM 20
#define BASE_BUILD0_ELERIUM 500
#define BASE_BUILD0_PLASMARIFLE 10
#define BASE_BUILD0_PLASMARIFLE_CLIP 100
#define BASE_BUILD0_FUSIONBALL_L 3
#define BASE_BUILD0_FUSIONBALL 50
/*----------------------------------------------------------------------------*/
#define BASE_MIN_PLASMABEAM 5
#define BASE_MIN_ELERIUM 100
#define BASE_MIN_PLASMARIFLE_CLIP 50
#define BASE_MIN_FUSIONBALL_L 1
#define BASE_MIN_FUSIONBALL 20
#define BASE_CHK_PLASMARIFLE_CLIP (BASE_MIN_PLASMARIFLE_CLIP/10)
/*----------------------------------------------------------------------------*/
#define BASE_FRESH_BUILDING       0x0001
#define BASE_FRESH_CRAFT_WEAPON   0x0010
#define BASE_FRESH_MATERIAL       0x0020
#define BASE_FRESH_SOLDIER_WEAPON 0x0040
#define BASE_FRESH_ALL0 (BASE_FRESH_CRAFT_WEAPON|BASE_FRESH_MATERIAL)
#define BASE_FRESH_ALL1 (BASE_FRESH_ALL0|BASE_FRESH_SOLDIER_WEAPON)
#define BASE_FRESH_ALL (BASE_FRESH_ALL1|BASE_FRESH_BUILDING)
/*----------------------------------------------------------------------------*/
#define base_fresh1(pb)  base_freshX(pb,BASE_FRESH_ALL)
/*----------------------------------------------------------------------------*/
void get_base_type(char* pstr, abyte type) {
	switch (type) {
		case BASE_TILE_ENTRANCE: strcpy(pstr,"Entrance"); break;
		case BASE_TILE_LIVING_QUARTERS: strcpy(pstr,"Living Quarters"); break;
		case BASE_TILE_LABORATORY: strcpy(pstr,"Laboratory"); break;
		case BASE_TILE_WORKSHOP: strcpy(pstr,"Workshop"); break;
		case BASE_TILE_SMALL_RADAR: strcpy(pstr,"Small Radar"); break;
		case BASE_TILE_LARGE_RADAR: strcpy(pstr,"Large Radar"); break;
		case BASE_TILE_GENERAL_STORES: strcpy(pstr,"General Stores"); break;
		case BASE_TILE_ALIEN_CONTAINMENT:
			strcpy(pstr,"Alien Containment"); break;
		case BASE_TILE_MISSILE_DEFENSE: strcpy(pstr,"Missile Defense"); break;
		case BASE_TILE_LASER_DEFENSE: strcpy(pstr,"Laser Defense"); break;
		case BASE_TILE_PLASMA_DEFENSE: strcpy(pstr,"Plasma Defense"); break;
		case BASE_TILE_FUSION_BALL_DEFENSE:
			strcpy(pstr,"Fusion Ball Defense"); break;
		case BASE_TILE_GRAVITY_SHIELD: strcpy(pstr,"Gravity Shield"); break;
		case BASE_TILE_MIND_SHIELD: strcpy(pstr,"Mind Shield"); break;
		case BASE_TILE_PSIONIC_LAB: strcpy(pstr,"Psionic Lab"); break;
		case BASE_TILE_HYPERWAVE_TRANSLATION:
			strcpy(pstr,"Hyperwave Translation"); break;
		case BASE_TILE_HANGAR_TL: strcpy(pstr,"Hangar"); break;
		default: pstr[0] = 0x0; break;
	}
}
/*----------------------------------------------------------------------------*/
void base_show_layout(base_t* base) {
	/* maybe not so useful? keeping this around for now... */
	char buff[32];
	int loop, irow, icol, temp;
	for (irow=0,loop=0;irow<6;irow++) {
		for (icol=0;icol<6;icol++,loop++) {
			temp = base->build_type[loop];
			if (temp!=0xff) {
				if ((temp&0xf0)>0x10||(temp>=0x14)) {
					fprintf(stderr,"** Invalid build tile @(%d,%d)[%d]:%02x\n",
						irow,icol,loop,temp);
				}
				else if (temp<=0x10) {
					get_base_type(buff,temp);
					printf("   > Build@(%d,%d)[%d]:%s",irow,icol,loop,buff);
					if (base->build_to_finish[loop])
						printf("<%d day(s)>",base->build_to_finish[loop]);
					printf("\n");
				}
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
int base_type_count(base_t* base, int type) {
	int loop, size;
	for (loop=0,size=0;loop<BASE_TILE_COUNT;loop++) {
		if (base->build_type[loop]==type)
			size++;
	}
	return size;
}
/*----------------------------------------------------------------------------*/
void base_show(base_t* base) {
	char buff[32];
	int type, size, step;
	printf("-- Base Name: %s\n",base->name);
	printf("   > Detection-S:%d  Detection-L:%d  Detection-H:%d\n",
		base->detection_short,base->detection_long,
		base->detection_hyperwave);
	if (base->count_engineer||base->count_scientist)
		printf("   > Engineers:%d  Scientists:%d\n",
			base->count_engineer,base->count_scientist);
	if (base->item_plasma_beam||base->elerium)
		printf("   > Plasma Beam:%d  Elerium:%d\n",
			base->item_plasma_beam,base->elerium);
	size = base_type_count(base,BASE_TILE_ENTRANCE);
	if (size!=1)
		fprintf(stderr,"** Invalid base entrance count? (%d)\n",size);
	for (type=1,step=0;type<=0x10;type++) {
		size = base_type_count(base,type);
		if (size>0) {
			get_base_type(buff,type);
			if (!step) printf("   #");
			printf(" [%s:%d]",buff,size);
			step++;
			if (step==3) {
				printf("\n");
				step = 0;
			}
		}
	}
	if (step) printf("\n");
}
/*----------------------------------------------------------------------------*/
void base_build0(base_t* base) {
	int loop;
	/* perfect build */
	const abyte build0[BASE_TILE_COUNT] = {
		BASE_TILE_FUSION_BALL_DEFENSE,BASE_TILE_FUSION_BALL_DEFENSE,
		BASE_TILE_HANGAR_TL,BASE_TILE_HANGAR_TR,
		BASE_TILE_WORKSHOP,BASE_TILE_ALIEN_CONTAINMENT,
		BASE_TILE_FUSION_BALL_DEFENSE,BASE_TILE_FUSION_BALL_DEFENSE,
		BASE_TILE_HANGAR_BL,BASE_TILE_HANGAR_BR,
		BASE_TILE_WORKSHOP,BASE_TILE_LABORATORY,
		BASE_TILE_HYPERWAVE_TRANSLATION,BASE_TILE_GRAVITY_SHIELD,
		BASE_TILE_ENTRANCE,BASE_TILE_LIVING_QUARTERS,
		BASE_TILE_WORKSHOP,BASE_TILE_LABORATORY,
		BASE_TILE_SMALL_RADAR,BASE_TILE_LARGE_RADAR,
		BASE_TILE_GENERAL_STORES,BASE_TILE_LIVING_QUARTERS,
		BASE_TILE_LIVING_QUARTERS,BASE_TILE_LIVING_QUARTERS,
		BASE_TILE_HANGAR_TL,BASE_TILE_HANGAR_TR,
		BASE_TILE_GENERAL_STORES,BASE_TILE_GENERAL_STORES,
		BASE_TILE_HANGAR_TL,BASE_TILE_HANGAR_TR,
		BASE_TILE_HANGAR_BL,BASE_TILE_HANGAR_BR,
		BASE_TILE_GENERAL_STORES,BASE_TILE_GENERAL_STORES,
		BASE_TILE_HANGAR_BL,BASE_TILE_HANGAR_BR
	};
	/* MUST BE existing! */
	if (!base->name[0]||base->empty_flag) return;
	/* create full base */
	for (loop=0;loop<BASE_TILE_COUNT;loop++) {
		base->build_type[loop] = build0[loop];
		base->build_to_finish[loop] = 0x0;
	}
	printf("-- Complete facilities built in '%s'!\n",base->name);
	/* adjust detection meter */
	base->detection_hyperwave = 0x64;
	base->detection_long = 0x64; /* def:20? */
	base->detection_short = 0x64; /* def:30? */
	/* provide 90 engineers and 100 scientists */
	base->count_engineer = 0x5A;
	base->count_scientist = 0x64;
	printf("-- %d engineers and %d scientists assigned to '%s'!\n",
		base->count_engineer,base->count_scientist,base->name);
	/* provide plasma beam craft weapon */
	base->item_plasma_beam = BASE_BUILD0_PLASMABEAM;
	printf("-- Plasma beam count:%d in '%s'!\n",
		base->item_plasma_beam,base->name);
	/* provide fusion ball craft weapon/ammo */
	base->item_fusion_ball_launcher = BASE_BUILD0_FUSIONBALL_L;
	printf("-- Fusion ball launcher count:%d in '%s'!\n",
		base->item_fusion_ball_launcher,base->name);
	base->item_fusion_ball = BASE_BUILD0_FUSIONBALL;
	printf("-- Fusion ball count:%d in '%s'!\n",
		base->item_fusion_ball,base->name);
	/* provide substantial amount of elerium */
	base->elerium = BASE_BUILD0_ELERIUM;
	printf("-- Elerium count:%d in '%s'!\n",base->elerium,base->name);
	base->item_plasma_beam = BASE_BUILD0_PLASMABEAM;
	/* provide soldier weapon/ammo - need research! */
	base->item_plasma_rifle = BASE_BUILD0_PLASMARIFLE;
	printf("-- Plasma rifle count:%d in '%s'!\n",
		base->item_plasma_rifle,base->name);
	base->item_plasma_rifle_clip = BASE_BUILD0_PLASMARIFLE_CLIP;
	printf("-- Plasma rifle clip count:%d in '%s'!\n",
		base->item_plasma_rifle_clip,base->name);
}
/*----------------------------------------------------------------------------*/
int base_freshX(base_t* base, unsigned int pick) {
	int stat = 0, loop, temp, done, scnt;
	char buff[32];
	if (!base->name[0]||base->empty_flag) /* must exists! */
		return stat;
	scnt = 0; /* count general store */
	/* finish all builds */
	if (pick&BASE_FRESH_BUILDING) {
		for (loop=0;loop<BASE_TILE_COUNT;loop++) {
			temp = base->build_type[loop];
			if (temp<0x14) {
				done = base->build_to_finish[loop];
				if (done>0&&done<0xff) {
					if (temp<=0x10) {
						get_base_type(buff,temp);
						printf("-- Completing building '%s' for '%s'!\n",
							buff,base->name);
					}
					base->build_to_finish[loop]= 0x0;
					stat++;
				}
			}
			if (temp==BASE_TILE_GENERAL_STORES) scnt++;
			else if (temp==BASE_TILE_SMALL_RADAR) {
				/* max short-range is actually 30 */
				if (base->detection_short<0x64) {
					printf("-- Short range detection:%d >> ",
						base->detection_short);
					base->detection_short=0x64;
					printf("%d! ('%s')\n",base->detection_short,base->name);
				}
			}
			else if (temp==BASE_TILE_LARGE_RADAR) {
				/* max long-range is actually 20 */
				if (base->detection_long<0x64) {
					printf("-- Long range detection:%d >> ",
						base->detection_long);
					base->detection_long=0x64;
					printf("%d! ('%s')\n",base->detection_long,base->name);
				}
			}
			else if (temp==BASE_TILE_HYPERWAVE_TRANSLATION) {
				if (base->detection_hyperwave<0x64) {
					printf("-- Hyperwave detection:%d >> ",
						base->detection_hyperwave);
					base->detection_hyperwave=0x64;
					printf("%d! ('%s')\n",
						base->detection_hyperwave,base->name);
					stat++;
				}
			}
		}
	}
	if (scnt>0) {
/*
	int step;
*/
		if (pick&BASE_FRESH_CRAFT_WEAPON) {
			/* provide some plasma beam craft weapon */
			if (!base->item_plasma_beam) {
				base->item_plasma_beam = BASE_MIN_PLASMABEAM;
				printf("-- Plasma beam count:%d in '%s'!\n",
					base->item_plasma_beam,base->name);
				stat++;
			}
		}
		if (pick&BASE_FRESH_MATERIAL) {
			/* provide some elerium */
			if (!base->elerium) {
				base->elerium = BASE_MIN_ELERIUM;
				printf("-- Elerium count:%d in '%s'!\n",
					base->elerium,base->name);
				stat++;
			}
		}
		if (pick&BASE_FRESH_SOLDIER_WEAPON) {
			/* provide some plasma rifle clip */
			if (base->item_plasma_rifle_clip&&
					base->item_plasma_rifle_clip<BASE_CHK_PLASMARIFLE_CLIP) {
				base->item_plasma_rifle_clip = BASE_MIN_PLASMARIFLE_CLIP;
				printf("-- Plasma rifle clip count:%d in '%s'!\n",
					base->item_plasma_rifle_clip,base->name);
				stat++;
			}
		}
		/* trying new item */
/*
		for (step=0,loop=0;step<4;step++) {
			if (base->unknown04[step]!=step+1) {
				base->unknown04[step] = step+1;
				stat++; loop++;
			}
		}
		if (loop) printf("@@ Trying new item(s)\n");
*/
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int base_load(base_t* base, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += BASE_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,BASE_DATA_NAME);
	pdat = fopen(buff,"rb");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk!=BASE_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			test = BASE_FULLCOUNT-offs;
			if (size>test) size = test;
			test = offs*BASE_INFO_SIZE;
			fseek(pdat,test,SEEK_SET);
			temp = fread((void*)base,BASE_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot read %s!\n",buff);
				stat = -3;
			}
			else stat = size;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
int base_save(base_t* base, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += BASE_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,BASE_DATA_NAME);
	pdat = fopen(buff,"rb+");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk!=BASE_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			test = BASE_FULLCOUNT-offs;
			if (size>test) size = test;
			test = offs*BASE_INFO_SIZE;
			fseek(pdat,test,SEEK_SET);
			temp = fwrite((void*)base,BASE_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot read %s!\n",buff);
				stat = -3;
			}
			else stat = size;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1XCOM1_BASE_H__ */
/*----------------------------------------------------------------------------*/
