/*----------------------------------------------------------------------------*/
#ifndef __MY1XCOM1_DATA_H__
#define __MY1XCOM1_DATA_H__
/*----------------------------------------------------------------------------*/
#include "xcom1_base.h"
#include "xcom1_merc.h"
/*----------------------------------------------------------------------------*/
/* ~$67M should be enough */
#define FUND_INIT_RICH 0x03ffffff
/* min threshold (~$5M) for topup (~$16M)*/
#define FUND_MIN_VALUE 0x4fffff
#define FUND_MIN_TOPUP 0xffffff
#define FUND_DATA_NAME "LIGLOB.DAT"
/*----------------------------------------------------------------------------*/
#define MAXCHAR_PATHFILE 512
/*----------------------------------------------------------------------------*/
typedef struct __xcom1_data_t {
	adwrd stat, flag;
	adwrd tpad;
	int fund;
	char path[MAXCHAR_PATHFILE];
	base_t base[BASE_FULLCOUNT];
	soldier_t soldier[SOLDIER_FULLCOUNT];
} xcom1_data_t;
/*----------------------------------------------------------------------------*/
#define XCOM1_STAT_EBUFFER  0x0010
#define XCOM1_STAT_EBASE    0x0020
#define XCOM1_STAT_EFUND    0x0040
#define XCOM1_STAT_ESOLDIER 0x0080
#define XCOM1_STAT_ERRORS0 (XCOM1_STAT_EBUFFER|XCOM1_STAT_EBASE)
#define XCOM1_STAT_ERRORS1 (XCOM1_STAT_EFUND|XCOM1_STAT_ESOLDIER)
#define XCOM1_STAT_ERRORS (XCOM1_STAT_ERRORS0|XCOM1_STAT_ERRORS1)
/*----------------------------------------------------------------------------*/
#define XCOM1_FLAG_INITIALIZE       0x00010000
#define XCOM1_FLAG_REFRESH          0x00020000
#define XCOM1_FLAG_COMMIT           0x00080000
#define XCOM1_FLAG_FRESH_BASE       0x1000
#define XCOM1_FLAG_FRESH_SOLDIER    0x2000
#define XCOM1_FLAG_FRESH_FUND       0x4000
#define XCOM1_FLAG_SHOW_ALL         0x0080
#define XCOM1_FLAG_SHOW_BASE        0x0001
#define XCOM1_FLAG_SHOW_SOLDIER     0x0002
#define XCOM1_FLAG_SHOW_MASK0 (XCOM1_FLAG_SHOW_BASE|XCOM1_FLAG_SHOW_SOLDIER)
#define XCOM1_FLAG_SHOW_MASK (XCOM1_FLAG_SHOW_ALL|XCOM1_FLAG_SHOW_MASK0)
#define XCOM1_FLAG_BASE_MODIFIED    0x0100
#define XCOM1_FLAG_FUND_MODIFIED    0x0200
#define XCOM1_FLAG_SOLDIER_MODIFIED 0x0400
/*----------------------------------------------------------------------------*/
#define XCOM1_FLAG_SUPER_SOLDIERS   0x4000
#define SOLDIER_SUPER0CNT 10
/*----------------------------------------------------------------------------*/
void xcom1_init(xcom1_data_t* xdat) {
	xdat->stat = 0;
	xdat->flag = 0;
	xdat->path[0] = 0x0;
}
/*----------------------------------------------------------------------------*/
void xcom1_show_base(xcom1_data_t* xdat) {
	int loop, step;
	for (loop=0,step=0;loop<BASE_FULLCOUNT;loop++) {
		if (xdat->base[loop].empty_flag||!xdat->base[loop].name[0]) continue;
		base_show(&xdat->base[loop]);
		step++;
	}
	if (step) printf("-- Base count:%d\n",step);
}
/*----------------------------------------------------------------------------*/
void xcom1_show_merc(xcom1_data_t* xdat) {
	int loop,step;
	for (loop=0,step=0;loop<SOLDIER_FULLCOUNT;loop++) {
		if (!xdat->soldier[loop].name[0]||
				xdat->soldier[loop].rank==SOLDIER_RANK_INVALID) continue;
		merc_show(&xdat->soldier[loop]);
		step++;
	}
	if (step) printf("-- Soldier count:%d\n",step);
}
/*----------------------------------------------------------------------------*/
int xcom1_load_base(xcom1_data_t* xdat, char* fullname) {
	FILE *pdat;
	long pchk, temp;
	int stat;
	pdat = fopen(fullname,"rb");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk!=BASE_FULLBYTES) {
			fprintf(stderr,"** Invalid %s!\n",fullname);
			stat = -2;
		}
		else {
			fseek(pdat,0,SEEK_SET);
			temp = fread((void*)xdat->base,BASE_INFO_SIZE,BASE_FULLCOUNT,pdat);
			if (temp!=BASE_FULLCOUNT) {
				fprintf(stderr,"** Cannot read %s!\n",fullname);
				stat = -3;
			}
			else stat = 0;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",fullname);
		stat = -1;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int xcom1_save_base(xcom1_data_t* xdat, char* fullname) {
	FILE *pdat;
	long temp;
	int stat;
	pdat = fopen(fullname,"wb");
	if (pdat) {
		temp = fwrite((void*)xdat->base,BASE_INFO_SIZE,BASE_FULLCOUNT,pdat);
		if (temp!=BASE_FULLCOUNT) {
			fprintf(stderr,"** Cannot write %s!\n",fullname);
			stat = -3;
		}
		else {
			printf("-- Base data saved to '%s'\n",fullname);
			stat = 0;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",fullname);
		stat = -1;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int xcom1_load_soldier(xcom1_data_t* xdat, char* fullname) {
	FILE *pdat;
	long pchk, temp;
	int stat;
	pdat = fopen(fullname,"rb");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk!=SOLDIER_FULLBYTES) {
			fprintf(stderr,"** Invalid %s!\n",fullname);
			stat = -2;
		}
		else {
			fseek(pdat,0,SEEK_SET);
			temp = fread((void*)xdat->soldier,SOLDIER_INFO_SIZE,
				SOLDIER_FULLCOUNT,pdat);
			if (temp!=SOLDIER_FULLCOUNT) {
				fprintf(stderr,"** Cannot read %s!\n",fullname);
				stat = -3;
			}
			else stat = 0;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",fullname);
		stat = -1;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int xcom1_save_soldier(xcom1_data_t* xdat, char* fullname) {
	FILE *pdat;
	long temp;
	int stat;
	pdat = fopen(fullname,"wb");
	if (pdat) {
		temp = fwrite((void*)xdat->soldier,SOLDIER_INFO_SIZE,
			SOLDIER_FULLCOUNT,pdat);
		if (temp!=SOLDIER_FULLCOUNT) {
			fprintf(stderr,"** Cannot write %s!\n",fullname);
			stat = -3;
		}
		else {
			printf("-- Soldier data saved to '%s'\n",fullname);
			stat = 0;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",fullname);
		stat = -1;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int xcom1_load_fund(xcom1_data_t* xdat, char* fullname) {
	FILE *pdat;
	long temp;
	int stat;
	pdat = fopen(fullname,"rb");
	if (pdat) {
		temp = fread((void*)&xdat->fund,sizeof(int),1,pdat);
		if (temp!=1) {
			fprintf(stderr,"** Cannot read %s!\n",fullname);
			stat = -3;
		}
		else stat = 0;
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",fullname);
		stat = -1;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int xcom1_save_fund(xcom1_data_t* xdat, char* fullname) {
	FILE *pdat;
	long temp;
	int stat;
	pdat = fopen(fullname,"wb");
	if (pdat) {
		temp = fwrite((void*)&xdat->fund,sizeof(int),1,pdat);
		if (temp!=1) {
			fprintf(stderr,"** Cannot write %s!\n",fullname);
			stat = -3;
		}
		else {
			printf("-- Fund data saved to '%s'\n",fullname);
			stat = 0;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",fullname);
		stat = -1;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
void xcom1_data_load(xcom1_data_t* xdat, char* fullpath) {
	char fullname[MAXCHAR_PATHFILE];
	int test;
	/* remove all error stats */
	xdat->stat &= ~XCOM1_STAT_ERRORS;
	/* save path */
	strncpy(xdat->path,fullpath,MAXCHAR_PATHFILE-1);
	xdat->path[MAXCHAR_PATHFILE-1] = '\0'; /* just in case */
	/* load base data */
	test = snprintf(fullname,MAXCHAR_PATHFILE,"%s/%s",fullpath,BASE_DATA_NAME);
	if (test>=MAXCHAR_PATHFILE) {
		fprintf(stderr,"** Name too long %s/%s! (%d)\n",
			fullpath,BASE_DATA_NAME,test);
		xdat->stat |= XCOM1_STAT_EBUFFER;
	}
	else if (xcom1_load_base(xdat,fullname)) {
		fprintf(stderr,"** Failed to load %s!\n",fullname);
		xdat->stat |= XCOM1_STAT_EBASE;
	}
	/* load fund data */
	test = snprintf(fullname,MAXCHAR_PATHFILE,"%s/%s",fullpath,FUND_DATA_NAME);
	if (test>=MAXCHAR_PATHFILE) {
		fprintf(stderr,"** Name too long %s/%s! (%d)\n",
			fullpath,FUND_DATA_NAME,test);
		xdat->stat |= XCOM1_STAT_EBUFFER;
	}
	else if (xcom1_load_fund(xdat,fullname)) {
		fprintf(stderr,"** Failed to load %s!\n",fullname);
		xdat->stat |= XCOM1_STAT_EFUND;
	}
	/* load soldier data */
	test = snprintf(fullname,MAXCHAR_PATHFILE,"%s/%s",
		fullpath,SOLDIER_DATA_NAME);
	if (test>=MAXCHAR_PATHFILE) {
		fprintf(stderr,"** Name too long %s/%s! (%d)\n",
			fullpath,SOLDIER_DATA_NAME,test);
		xdat->stat |= XCOM1_STAT_EBUFFER;
	}
	else if (xcom1_load_soldier(xdat,fullname)) {
		fprintf(stderr,"** Failed to load %s!\n",fullname);
		xdat->stat |= XCOM1_STAT_ESOLDIER;
	}
}
/*----------------------------------------------------------------------------*/
void xcom1_data_show(xcom1_data_t* xdat) {
	if (xdat->flag&XCOM1_FLAG_SHOW_MASK)
		printf("-- Funds:$%d\n",xdat->fund);
	if (xdat->flag&(XCOM1_FLAG_SHOW_ALL|XCOM1_FLAG_SHOW_BASE))
		xcom1_show_base(xdat);
	if (xdat->flag&(XCOM1_FLAG_SHOW_ALL|XCOM1_FLAG_SHOW_SOLDIER))
		xcom1_show_merc(xdat);
}
/*----------------------------------------------------------------------------*/
void xcom1_data_hack(xcom1_data_t* xdat) {
	int loop;
	/* only hack if no error status */
	if (xdat->stat&XCOM1_STAT_ERRORS) return;
	if (xdat->flag&XCOM1_FLAG_INITIALIZE) {
		if (xdat->fund<FUND_INIT_RICH) {
			printf("-- Funds increased from %d",xdat->fund);
			xdat->fund = FUND_INIT_RICH;
			printf("to %d!\n",xdat->fund);
			xdat->flag |= XCOM1_FLAG_FUND_MODIFIED;
		}
		for (loop=0;loop<BASE_FULLCOUNT;loop++) {
			if (!xdat->base[loop].name[0]||xdat->base[loop].empty_flag)
				continue;
			base_build0(&xdat->base[loop]);
			xdat->flag |= XCOM1_FLAG_BASE_MODIFIED;
		}
		if (xdat->flag&XCOM1_FLAG_SUPER_SOLDIERS) {
			for (loop=0;loop<SOLDIER_SUPER0CNT;loop++) {
				merc_super0(&xdat->soldier[loop]);
				xdat->flag |= XCOM1_FLAG_SOLDIER_MODIFIED;
			}
		}
		else {
			for (loop=0;loop<SOLDIER_FULLCOUNT;loop++) {
				if (!xdat->soldier[loop].name[0]||
						xdat->soldier[loop].rank==MERC_NONE)
					continue;
				merc_build0(&xdat->soldier[loop]);
				xdat->flag |= XCOM1_FLAG_SOLDIER_MODIFIED;
			}
		}
	}
	else {
		if (xdat->flag&(XCOM1_FLAG_FRESH_FUND|XCOM1_FLAG_REFRESH)) {
			if (xdat->fund<FUND_MIN_VALUE) {
				printf("-- Funds increased from %d",xdat->fund);
				xdat->fund = FUND_MIN_TOPUP;
				printf("to %d!\n",xdat->fund);
				xdat->flag |= XCOM1_FLAG_FUND_MODIFIED;
			}
		}
		if (xdat->flag&(XCOM1_FLAG_FRESH_BASE|XCOM1_FLAG_REFRESH)) {
			for (loop=0;loop<BASE_FULLCOUNT;loop++) {
				if (!xdat->base[loop].name[0]||xdat->base[loop].empty_flag)
					continue;
				if (base_fresh1(&xdat->base[loop]))
					xdat->flag |= XCOM1_FLAG_BASE_MODIFIED;
			}
		}
		if (xdat->flag&(XCOM1_FLAG_FRESH_SOLDIER|XCOM1_FLAG_REFRESH)) {
			for (loop=0;loop<SOLDIER_FULLCOUNT;loop++) {
				if (!xdat->soldier[loop].name[0]||
					xdat->soldier[loop].rank==MERC_NONE)
					continue;
				if (merc_fresh1(&xdat->soldier[loop]))
					xdat->flag |= XCOM1_FLAG_SOLDIER_MODIFIED;
			}
		}
	}
}
/*----------------------------------------------------------------------------*/
void xcom1_data_save(xcom1_data_t* xdat) {
	char fullname[MAXCHAR_PATHFILE];
	int test;
	/* must explicitly ask to commit */
	if (!(xdat->flag&XCOM1_FLAG_COMMIT)) return;
	/* only save if no error status */
	if (xdat->stat&XCOM1_STAT_ERRORS) return;
	/* save base data */
	if (xdat->flag&XCOM1_FLAG_BASE_MODIFIED) {
		test = snprintf(fullname,MAXCHAR_PATHFILE,"%s/%s",
			xdat->path,BASE_DATA_NAME);
		if (test>=MAXCHAR_PATHFILE) {
			fprintf(stderr,"** Name too long %s/%s! (%d)\n",
				xdat->path,BASE_DATA_NAME,test);
		}
		else xcom1_save_base(xdat,fullname);
	}
	/* save fund data */
	if (xdat->flag&XCOM1_FLAG_FUND_MODIFIED) {
		test = snprintf(fullname,MAXCHAR_PATHFILE,"%s/%s",
			xdat->path,FUND_DATA_NAME);
		if (test>=MAXCHAR_PATHFILE) {
			fprintf(stderr,"** Name too long %s/%s! (%d)\n",
				xdat->path,FUND_DATA_NAME,test);
			xdat->stat |= XCOM1_STAT_EBUFFER;
		}
		else xcom1_save_fund(xdat,fullname);
	}
	/* save soldier data */
	if (xdat->flag&XCOM1_FLAG_SOLDIER_MODIFIED) {
		test = snprintf(fullname,MAXCHAR_PATHFILE,"%s/%s",
			xdat->path,SOLDIER_DATA_NAME);
		if (test>=MAXCHAR_PATHFILE) {
			fprintf(stderr,"** Name too long %s/%s! (%d)\n",
				xdat->path,SOLDIER_DATA_NAME,test);
		}
		else xcom1_save_soldier(xdat,fullname);
	}
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1XCOM1_DATA_H__ */
/*----------------------------------------------------------------------------*/
