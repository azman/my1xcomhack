/*----------------------------------------------------------------------------*/
#ifndef __MY1XCOM1_MERC_H__
#define __MY1XCOM1_MERC_H__
/*----------------------------------------------------------------------------*/
#include "my1type.h"
/*----------------------------------------------------------------------------*/
typedef struct __soldier {
	aword rank;
	/** base location ref (first xcom base is always 0) */
	aword base; /* 0xFFFF in transfer? */
	aword craft; /* ref index in LOC.DAT */
	aword craft_prev; /** craft before wounded */
	aword missions;
	aword kills;
	aword recovery;
/* [20250223] from:https://www.ufopaedia.org/index.php?title=SOLDIER.DAT
value: victory point loss if died in a mission [init=20@0x14]
squaddie(+1),sergeant(+1),captain(+3),colonel(+6),commander(+10)
so, keep this low?
*/
	aword xfactor;
	abyte name[26]; /* 25+1 (only 21 can be seen using in-game editor) */
	abyte time_unit, health, stamina, reaction, strength, firing_accuracy;
	abyte throwing_accuracy, closecombat_accuracy, psi_strength, psi_skill;
	abyte bravery, TU_plus;
	abyte HEA_plus, STA_plus, RCT_plus, STR_plus, FA_plus, TA_plus;
	abyte CCA_plus, BRV_plus;
	abyte armor;
	abyte psi_training; /* remaining months? */
	abyte psi_train; /* flag 0/1 */
	abyte promotion; /* flag 0/1 */
	abyte gender; /* 0=male 1=female */
	abyte appear; /* only 4 (0=blonde,1=brown,2=Oriental,3=African) */
} soldier_t;
/*----------------------------------------------------------------------------*/
typedef soldier_t merc_t;
/*----------------------------------------------------------------------------*/
#define SOLDIER_INFO_SIZE 68
#define SOLDIER_FULLCOUNT 250
#define SOLDIER_FULLBYTES (SOLDIER_INFO_SIZE*SOLDIER_FULLCOUNT)
#define SOLDIER_DATA_NAME "SOLDIER.DAT"
#define SOLDIER_DATA_NAMESZ 11
/*----------------------------------------------------------------------------*/
#define MERC_INFO_SIZE SOLDIER_INFO_SIZE
#define MERC_FULLCOUNT SOLDIER_FULLCOUNT
/*----------------------------------------------------------------------------*/
#define SOLDIER_BRAVERY(x) (int)((110-x)/10)
/*----------------------------------------------------------------------------*/
#define SOLDIER_RANK_INVALID 0xFFFF
#define SOLDIER_RANK_ROOKIE 0x0000
#define SOLDIER_RANK_SQUADDIE 0x0001
#define SOLDIER_RANK_SERGEANT 0x0002
#define SOLDIER_RANK_CAPTAIN 0x0003
#define SOLDIER_RANK_COLONEL 0x0004
#define SOLDIER_RANK_COMMANDER 0x0005
#define SOLDIER_ARMOR_NONE 0x00
#define SOLDIER_ARMOR_PERSONAL_ARMOR 0x01
#define SOLDIER_ARMOR_POWER_SUIT 0x02
#define SOLDIER_ARMOR_FLYING_SUIT 0x03
/*----------------------------------------------------------------------------*/
#define MERC_ROOKIE SOLDIER_RANK_ROOKIE
#define MERC_SQUADDIE SOLDIER_RANK_SQUADDIE
#define MERC_SERGEANT SOLDIER_RANK_SERGEANT
#define MERC_CAPTAIN SOLDIER_RANK_CAPTAIN
#define MERC_COLONEL SOLDIER_RANK_COLONEL
#define MERC_COMMANDER SOLDIER_RANK_COMMANDER
/*----------------------------------------------------------------------------*/
#define MERC_XFACTOR_XPRANK0 0x14
#define MERC_XFACTOR_XPRANK4 0x1E
/*----------------------------------------------------------------------------*/
/* not sure where the line is between ranks? */
#define MERC_XFACTOR_XPRANK1 0x18
#define MERC_XFACTOR_XPRANK2 0x19
#define MERC_XFACTOR_XPRANK3 0x1B
/*----------------------------------------------------------------------------*/
#define MERC_XFACTOR_ROOKIE MERC_XFACTOR_XPRANK0
/*----------------------------------------------------------------------------*/
#define MERC_STAT_FULL 0x64
#define MERC_PLUS_FULL 0x32
/*----------------------------------------------------------------------------*/
#define MERC_NONE SOLDIER_RANK_INVALID
#define MERC_SUPER0_NAME "Maverick"
#define MERC_BRAVERY(x) SOLDIER_BRAVERY(x)
#define MERC_BRAVE_X(x) (int)(x/10)
/*----------------------------------------------------------------------------*/
#define MERC(pm) ((merc_t*)pm)
#define merc_null(pm) (MERC(pm)->rank==MERC_NONE)
#define merc_name(pm) (MERC(pm)->name[0])
#define merc_duty(pm) (MERC(pm)->missions)
#define merc_xval(pm) ((int)(MERC(pm)->xfactor)>=MERC_XFACTOR_ROOKIE)
#define merc_died(pm) (merc_name(pm)&&merc_duty(pm)&&merc_xval(pm))
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#define merc_show(pm) merc_show_core(MERC(pm),"-- Soldier: ")
/*----------------------------------------------------------------------------*/
void merc_show_info(merc_t* merc) {
	if (merc->name[0])
		printf("%s ",merc->name);
	printf("(");
	if (!merc_null(merc)) {
		if (merc->gender&0xfe) /* not 0:male nor 1:female */
			printf("0x%02x:",merc->gender);
		else printf("%c:",(merc->gender?'F':'M'));
	}
	switch (merc->rank) {
		case MERC_ROOKIE: printf("Rookie"); break;
		case MERC_SQUADDIE: printf("Squaddie"); break;
		case MERC_SERGEANT: printf("Sergeant"); break;
		case MERC_CAPTAIN: printf("Captain"); break;
		case MERC_COLONEL: printf("Colonel"); break;
		case MERC_COMMANDER: printf("Commander"); break;
		default: printf("0x%04X",merc->rank); break;
	}
	printf(")");
}
/*----------------------------------------------------------------------------*/
void merc_show_loca(merc_t* merc) {
	printf(" Base:0x%04X",merc->base);
	if (merc->craft!=0xffff)
		printf(" Craft:0x%04X",merc->craft);
	if (merc->craft_prev!=0xffff)
		printf(" CraftPrev:0x%04X",merc->craft_prev);
}
/*----------------------------------------------------------------------------*/
void merc_show_face(merc_t* merc) {
	printf(" [V:0x%02x]\n",merc->appear);
}
/*----------------------------------------------------------------------------*/
void merc_show_stat(merc_t* merc) {
	if (merc->recovery)
		printf("   ** Wounded:%d day(s) to recover\n",merc->recovery);
	if (merc->psi_train)
		printf("   @@ In PSI training:%d month(s)\n",merc->psi_training);
	printf("   > Mission(s):%d, Kill(s):%d, X-factor:%d\n",
		merc->missions,merc->kills,merc->xfactor);
	printf("   > TU:%d(+%d), Strength:%d(+%d), Stamina:%d(+%d)\n",
		merc->time_unit,merc->TU_plus,merc->strength,merc->STR_plus,
		merc->stamina,merc->STA_plus);
	printf("   > Health:%d(+%d), Reaction:%d(+%d), Bravery:%d(+%d)\n",
		merc->health,merc->HEA_plus,merc->reaction,merc->RCT_plus,
		merc->bravery,merc->BRV_plus);
	printf("   > Firing Acc.:%d(+%d), Throwing Acc.:%d(+%d)\n",
		merc->firing_accuracy,merc->FA_plus,
		merc->throwing_accuracy,merc->TA_plus);
	printf("   > Close-combat Acc.:%d(+%d), PSI Skill:%d, PSI Strength:%d\n",
		merc->closecombat_accuracy,merc->CCA_plus,
		merc->psi_skill,merc->psi_strength);
}
/*----------------------------------------------------------------------------*/
void merc_show_core(merc_t* merc, char* pstr) {
	if (pstr) printf(pstr);
	merc_show_info(merc);
	merc_show_loca(merc);
	merc_show_face(merc);
	merc_show_stat(merc);
}
/*----------------------------------------------------------------------------*/
int merc_build0(merc_t* merc) {
	int stat = 0;
	if (merc->rank==MERC_NONE) return stat;
	if (merc->time_unit<MERC_STAT_FULL) {
		merc->time_unit = MERC_STAT_FULL;
		stat++;
	}
	if (merc->health<MERC_STAT_FULL) {
		merc->health = MERC_STAT_FULL;
		stat++;
	}
	if (merc->stamina<MERC_STAT_FULL) {
		merc->stamina = MERC_STAT_FULL;
		stat++;
	}
	if (merc->reaction<MERC_STAT_FULL) {
		merc->reaction = MERC_STAT_FULL;
		stat++;
	}
	if (merc->strength<MERC_STAT_FULL) {
		merc->strength = MERC_STAT_FULL;
		stat++;
	}
	if (merc->firing_accuracy<MERC_STAT_FULL) {
		merc->firing_accuracy = MERC_STAT_FULL;
		stat++;
	}
	if (merc->throwing_accuracy<MERC_STAT_FULL) {
		merc->throwing_accuracy = MERC_STAT_FULL;
		stat++;
	}
	if (merc->closecombat_accuracy<MERC_STAT_FULL) {
		merc->closecombat_accuracy = MERC_STAT_FULL;
		stat++;
	}

	if (merc->psi_strength<MERC_STAT_FULL) {
		merc->psi_strength = MERC_STAT_FULL;
		stat++;
	}
	if (merc->psi_skill<MERC_STAT_FULL) {
		merc->psi_skill = MERC_STAT_FULL;
		stat++;
	}
	if (merc->bravery!=SOLDIER_BRAVERY(100)) {
		merc->bravery = SOLDIER_BRAVERY(100);
		stat++;
	}
	if (merc->armor!=SOLDIER_ARMOR_FLYING_SUIT) {
		merc->armor = SOLDIER_ARMOR_FLYING_SUIT;
		stat++;
	}
	if (stat) printf("-- Full stat(s) for %s\n",(char*)merc->name);
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_buildX(merc_t* merc) {
	int stat = 0;
	if (merc->rank==MERC_NONE) return stat;
	if (merc->TU_plus<MERC_PLUS_FULL) {
		merc->TU_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->HEA_plus<MERC_PLUS_FULL) {
		merc->HEA_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->STA_plus<MERC_PLUS_FULL) {
		merc->STA_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->RCT_plus<MERC_PLUS_FULL) {
		merc->RCT_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->STR_plus<MERC_PLUS_FULL) {
		merc->STR_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->FA_plus<MERC_PLUS_FULL) {
		merc->FA_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->TA_plus<MERC_PLUS_FULL) {
		merc->TA_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->CCA_plus<MERC_PLUS_FULL) {
		merc->CCA_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->BRV_plus<MERC_BRAVE_X(MERC_PLUS_FULL)) {
		merc->BRV_plus = MERC_BRAVE_X(MERC_PLUS_FULL);
		stat++;
	}
	if (stat) printf("-- PLUS stat(s) for %s\n",(char*)merc->name);
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_revive(merc_t* merc) {
	int stat = 0;
	if (merc_died(merc)) {
		printf("-- Resurrecting %s as rookie!\n",(char*)merc->name);
		merc->rank = MERC_ROOKIE;
		merc->xfactor = MERC_XFACTOR_ROOKIE;
		stat++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_fresh1(merc_t* merc) {
	int stat = 0;
	if (merc->rank==MERC_NONE) {
		if (merc_revive(merc)) stat++;
		else return stat;
	}
	stat += merc_build0(merc);
	if (merc->recovery) {
		merc->recovery = 0;
		printf("-- Full recovery for wounded %s!\n",(char*)merc->name);
		merc->craft = merc->craft_prev;
		merc->craft_prev = 0xffff;
		stat++;
	}
	if (merc->bravery!=MERC_BRAVERY(MERC_STAT_FULL)) {
		merc->bravery = MERC_BRAVERY(MERC_STAT_FULL);
		printf("-- Restoring bravery for %s!\n",(char*)merc->name);
		stat++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_domake(merc_t* merc, char *name) {
	char buff[] = MERC_SUPER0_NAME;
	int stat = 0;
	/* works on 'empty' slot */
	if (merc->rank==MERC_NONE) {
		merc->rank = MERC_ROOKIE;
		if (!name) name = buff;
		strcpy((char*)merc->name,MERC_SUPER0_NAME);
		merc->xfactor = MERC_XFACTOR_ROOKIE;
		merc->craft = 0xFFFF; /* new hires gets no craft assignment */
		stat++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_super0(merc_t* merc) {
	int stat;
	stat = merc_domake(merc,MERC_SUPER0_NAME);
	if (stat) printf("-- Training ");
	else printf("-- Upgrading ");
	printf("%s as super soldier!\n",(char*)merc->name);
	if (merc->rank!=MERC_COMMANDER) {
		printf("## Promoted to Commander! (0x%x >> ",merc->rank);
		merc->rank = MERC_COMMANDER;
		printf("0x%x)\n",merc->rank);
		stat++;
	}
	stat += merc_build0(merc);
	stat += merc_buildX(merc);
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_load(merc_t* merc, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += SOLDIER_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,SOLDIER_DATA_NAME);
	pdat = fopen(buff,"rb");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk!=SOLDIER_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			test = SOLDIER_FULLCOUNT-offs;
			if (size>test) size = test;
			test = offs*SOLDIER_INFO_SIZE;
			fseek(pdat,test,SEEK_SET);
			temp = fread((void*)merc,SOLDIER_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot read %s!\n",buff);
				stat = -3;
			}
			else stat = size;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_save(merc_t* merc, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += SOLDIER_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,SOLDIER_DATA_NAME);
	pdat = fopen(buff,"rb+"); /* w is not ok if 'editing' */
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk!=SOLDIER_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			test = SOLDIER_FULLCOUNT-offs;
			if (size>test) size = test;
			test = offs*SOLDIER_INFO_SIZE;
			fseek(pdat,test,SEEK_SET);
			temp = fwrite((void*)merc,SOLDIER_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot write %s!\n",buff);
				stat = -3;
			}
			else stat = size;
		}
		fseek(pdat,0,SEEK_END); /* do i need this? */
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1XCOM1_MERC_H__ */
/*----------------------------------------------------------------------------*/
