/*----------------------------------------------------------------------------*/
#ifndef __MY1XCOM1_LOCD_H__
#define __MY1XCOM1_LOCD_H__
/*----------------------------------------------------------------------------*/
#include "my1type.h"
/*----------------------------------------------------------------------------*/
/**
 * location/container data?
 * - purely info from https://www.ufopaedia.org/index.php?title=LOC.DAT
 * - do i need this?
**/
/*----------------------------------------------------------------------------*/
typedef struct __locd_t {
	abyte type;
	abyte objr; /* object refs: e.g. ship(CRAFT.DAT) xcombase(BASE.DAT) */
	aword horz; /* horizontal coord: 0 to 2880 */
	aword vert; /* vertical coord: -720 to 720 */
	/**
	 * count down timer
	 * - crash/terror site: hours exists?
	 * - moving object: game-ticks (5s) to target (cellsize/speed)
	**/
	aword cnt0;
	/* fraction of cnt0 for moving object (cellsize%speed) */
	aword cnt1;
	aword idx1; /* base-1 index for item */
	abyte notused00[2];
	abyte tmod; /* transfer mode 0x01:in transfer 0x02:transfer select bit */
	abyte notused01;
	abyte unknown00[4]; /* visibility@mobility bit fields? */
} locd_t;
/*----------------------------------------------------------------------------*/
#define LOCD_INFO_SIZE 20
#define LOCD_FULLCOUNT 50
#define LOCD_FULLBYTES (LOCD_INFO_SIZE*LOCD_FULLCOUNT)
#define LOCD_DATA_NAME "LOC.DAT"
#define LOCD_DATA_NAMESZ 7
/*----------------------------------------------------------------------------*/
#define LOCD_TYPE_UNUSED 0x00
#define LOCD_TYPE_ALIENSHIP_MOVING 0x01
#define LOCD_TYPE_XCOMSHIP 0x02
#define LOCD_TYPE_XCOMBASE 0x03
#define LOCD_TYPE_ALIENBASE 0x04
#define LOCD_TYPE_CRASHSITE 0x05
#define LOCD_TYPE_ALIENSHIP_LANDED 0x06
#define LOCD_TYPE_WAYPOINT 0x07
#define LOCD_TYPE_TERRORSITE 0x08
/*
in xcom2: 0x08 not used
0x51 - Port Attack
0x52 - Island Attack
0x53 - Passenger/Cargo Ship
0x54 - Artefact Site
*/
/*----------------------------------------------------------------------------*/
#define LOCD_OBJR_ALIENBASE_SECTOID 0x00
#define LOCD_OBJR_ALIENBASE_SNAKEMAN 0x01
#define LOCD_OBJR_ALIENBASE_ETHEREAL 0x02
#define LOCD_OBJR_ALIENBASE_MUTON 0x03
#define LOCD_OBJR_ALIENBASE_FLOATER 0x04
#define LOCD_OBJR_ALIENBASE_CYDONIA 0x05
/*
in xcom2: aquatoid,gillman,lobsterman,tasoth,mixed1,mixed2
*/
/*----------------------------------------------------------------------------*/
void locd_show(locd_t* locd) {
	if (locd->type==LOCD_TYPE_UNUSED) return;
	printf("Type: ");
	switch (locd->type) {
		case LOCD_TYPE_XCOMBASE:
			printf("XCOM Base-%d",(int)locd->objr+1);
			/* locd->idx1 something else for XCOM base! */
			break;
		case LOCD_TYPE_XCOMSHIP:
			printf("XCOM Craft [CRAFT.DAT:%d]",(int)locd->objr);
			printf(" [Type-%d]",locd->idx1);
			break;
		case LOCD_TYPE_ALIENBASE:
			printf("Alien Base-%d ",locd->idx1);
			switch (locd->objr) {
				case LOCD_OBJR_ALIENBASE_SECTOID:
					printf("{Sectoid}"); break;
				case LOCD_OBJR_ALIENBASE_SNAKEMAN:
					printf("{Snakeman}"); break;
				case LOCD_OBJR_ALIENBASE_ETHEREAL:
					printf("{Ethereal}"); break;
					break;
				case LOCD_OBJR_ALIENBASE_MUTON:
					printf("{Muton}"); break;
					break;
				case LOCD_OBJR_ALIENBASE_FLOATER:
					printf("{Floater}"); break;
					break;
				case LOCD_OBJR_ALIENBASE_CYDONIA:
					printf("{Cydonia???}"); break;
					break;
				default: printf("{???:%02X}",locd->objr); break;
			}
			break;
		case LOCD_TYPE_ALIENSHIP_MOVING:
			printf("Alien Ship-%d [CRAFT.DAT:%d]",locd->idx1,locd->objr);
			break;
		case LOCD_TYPE_ALIENSHIP_LANDED:
			printf("Alien Ship-%d (Ground) [CRAFT.DAT:%d]",
				locd->idx1,locd->objr);
			break;
		case LOCD_TYPE_CRASHSITE:
			printf("Alien Crash Site-%d (%dH)",locd->idx1,locd->cnt0);
			break;
		case LOCD_TYPE_TERRORSITE:
			printf("Terror Site-%d (%dH)",locd->idx1,locd->cnt0);
			break;
		case LOCD_TYPE_WAYPOINT:
			printf("Waypoint-%d",locd->idx1);
			break;
		default: printf("[???:%02X]",locd->type); break;
	}
	printf(" @Pos:(%d,%d)\n",(short int)locd->horz,(short int)locd->vert);
}
/*----------------------------------------------------------------------------*/
int locd_load(locd_t* locd, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp, full;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += LOCD_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,LOCD_DATA_NAME);
	pdat = fopen(buff,"rb");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk%LOCD_INFO_SIZE||pchk>LOCD_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			full = pchk/LOCD_INFO_SIZE;
			if (offs<full) {
				test = full-offs;
				if (size>test) size = test;
				test = offs*LOCD_INFO_SIZE;
				fseek(pdat,test,SEEK_SET);
				temp = fread((void*)locd,LOCD_INFO_SIZE,size,pdat);
				if (temp!=size) {
					fprintf(stderr,"** Cannot read %s!\n",buff);
					stat = -3;
				}
				else stat = size;
			}
			else stat = -4;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
int locd_save(locd_t* locd, char* path, int size) {
	FILE *pdat;
	char *buff;
	long pchk, temp, full;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += LOCD_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,LOCD_DATA_NAME);
	pdat = fopen(buff,"rb+"); /* w is not ok if 'editing' */
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk%LOCD_INFO_SIZE||pchk>LOCD_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			full = pchk/LOCD_INFO_SIZE;
			if (size>full) size = full;
			fseek(pdat,0,SEEK_SET);
			temp = fwrite((void*)locd,LOCD_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot write %s!\n",buff);
				stat = -3;
			}
			else stat = size;
		}
		fseek(pdat,0,SEEK_END); /* do i need this? */
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1XCOM1_LOCD_H__ */
/*----------------------------------------------------------------------------*/
