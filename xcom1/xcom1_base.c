/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define DEFAULT_SAVEPATH "."
#define MAXCHAR_PATHFILE 512
/*----------------------------------------------------------------------------*/
#define FLAG_DOSAVE 0x80
#define FLAG_HACK00 0x08
#define FLAG_BUILD0 0x04
#define FLAG_FRESH1 0x01
#define FLAG_HACKED (FLAG_BUILD0|FLAG_FRESH1|FLAG_HACK00)
/*----------------------------------------------------------------------------*/
#include "xcom1_base.h"
/*----------------------------------------------------------------------------*/
#define BASE_ITEM_NAMELEN 32
/*----------------------------------------------------------------------------*/
typedef struct __base_item_t {
	int offs, more;
	char name[BASE_ITEM_NAMELEN];
} base_item_t;
/*----------------------------------------------------------------------------*/
#define BASE_ITEM_OFFS_PLASMA_BEAM 5
#define BASE_ITEM_OFFS_PLASMA_RIFLE 51
#define BASE_ITEM_OFFS_PLASMA_RIFLE_CLIP 52
#define BASE_ITEM_OFFS_ELERIUM 60
/*----------------------------------------------------------------------------*/
base_item_t base_item_db[] = {
	{ BASE_ITEM_OFFS_PLASMA_BEAM,0,"Plasma beam" },
	{ BASE_ITEM_OFFS_PLASMA_RIFLE,0,"Plasma rifle" },
	{ BASE_ITEM_OFFS_PLASMA_RIFLE_CLIP,0,"Plasma clip" },
	{ BASE_ITEM_OFFS_ELERIUM,0,"Elerium-115" },
	{ -1,0,"" }
};
/*----------------------------------------------------------------------------*/
#define BASE(pb) ((base_t*)pb)
#define BASE_ITEM(pb) ((aword*)&BASE(pb)->item_stingray_launcher)
/*----------------------------------------------------------------------------*/
int base_supply_pick(base_t* base, base_item_t* pick) {
	aword* that;
	int stat;
	stat = 0;
	if (pick) {
		that = BASE_ITEM(base);
		if (that[pick->offs]<pick->more&&pick->more>0) {
			printf("-- Supply:{%s} %d >> ",pick->name,that[pick->offs]);
			that[pick->offs] = pick->more;
			printf("%d in '%s'!\n",that[pick->offs],base->name);
			stat++;
		}
		else
			printf("-- Current:{%s:%d} in '%s'!\n",
				pick->name,that[pick->offs],base->name);
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
base_item_t * base_supply_item(int item) {
	base_item_t *pick;
	pick = base_item_db;
	while (pick->offs>=0) {
		if (pick->offs==item)
			break;
		pick++;
	}
	if (pick->offs<0) pick = 0x0;
	return pick;
}
/*----------------------------------------------------------------------------*/
int base_supply(base_t* base, int item, int icnt) {
	base_item_t *pick;
	int stat;
	stat = 0;
	/* validate picked supply */
	pick = base_supply_item(item);
	if (pick) {
		if (icnt>0) {
			pick->more = icnt;
			stat = base_supply_pick(base,pick);
		}
		else pick->more = 0;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int base_supply_more(base_t* base) {
	base_item_t *pick;
	int stat;
	stat = 0;
	pick = base_item_db;
	while (pick->offs>=0) {
		if (pick->more)
			stat += base_supply_pick(base,pick);
		pick++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	base_t base[BASE_FULLCOUNT];
	base_item_t *item;
	int loop, test, pick, offs, size, flag;
	char *path, *ptmp, *pchk;
	char full[MAXCHAR_PATHFILE];
	char pdef[] = DEFAULT_SAVEPATH;
	path = pdef;
	/* validate! */
	if (sizeof(base_t)!=BASE_INFO_SIZE) {
		fprintf(stderr,"** Invalid Base data structure! (%d|%d)\n",
			(int)sizeof(base_t),(int)BASE_INFO_SIZE);
		return -1;
	}
	/* default setting */
	pick = 1; offs = 0; size = BASE_FULLCOUNT; flag = 0;
	/* check program args */
	if (argc>0) {
		for (loop=1; loop<argc; loop++) {
			pchk = argv[loop];
			if (pchk[0]=='-') {
				if (pchk[1]=='-') {
					ptmp = &pchk[2];
					if (strcmp(ptmp,"offs")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<0||test>=BASE_FULLCOUNT)
							fprintf(stderr,"** Invalid offset!\n");
						else offs = test;
					}
					else if (strcmp(ptmp,"size")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>(BASE_FULLCOUNT-offs))
							fprintf(stderr,"** Invalid size!\n");
						else size = test;
					}
					else if (strcmp(ptmp,"elerium")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>BASE_BUILD0_ELERIUM)
							fprintf(stderr,"** Invalid number for %s!\n",
								argv[loop-1]);
						else {
							item = base_supply_item(BASE_ITEM_OFFS_ELERIUM);
							if (item) {
								item->more = test;
								flag |= FLAG_HACK00;
							}
						}
					}
					else if (strcmp(ptmp,"plasma-beam")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>BASE_BUILD0_PLASMABEAM)
							fprintf(stderr,"** Invalid number for %s!\n",
								argv[loop-1]);
						else {
							item = base_supply_item(BASE_ITEM_OFFS_PLASMA_BEAM);
							if (item) {
								item->more = test;
								flag |= FLAG_HACK00;
							}
						}
					}
					else if (strcmp(ptmp,"plasma-rifle")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>BASE_BUILD0_PLASMARIFLE)
							fprintf(stderr,"** Invalid number for %s!\n",
								argv[loop-1]);
						else {
							item =
								base_supply_item(BASE_ITEM_OFFS_PLASMA_RIFLE);
							if (item) {
								item->more = test;
								flag |= FLAG_HACK00;
							}
						}
					}
					else if (strcmp(ptmp,"plasma-rifleclip")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>BASE_BUILD0_PLASMARIFLE_CLIP)
							fprintf(stderr,"** Invalid number for %s!\n",
								argv[loop-1]);
						else {
							item = base_supply_item(
								BASE_ITEM_OFFS_PLASMA_RIFLE_CLIP);
							if (item) {
								item->more = test;
								flag |= FLAG_HACK00;
							}
						}
					}
					else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
				}
				else if (!pchk[2]) {
					if (pchk[1]=='g') {
						if (++loop==argc) break;
						pick = atoi(argv[loop]);
						if (pick<0||pick>10) {
							fprintf(stderr,"** Invalid game index (1-10)!\n");
							pick = 1;
						}
					}
					else if (pchk[1]=='p') {
						if (++loop==argc) break;
						path = argv[loop];
					}
					else if (pchk[1]=='i') {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>BASE_FULLCOUNT) {
							fprintf(stderr,"** Invalid base index (1-%d)!\n",
								BASE_FULLCOUNT);
						}
						else {
							offs = test-1;
							size = 1;
						}
					}
					else if (pchk[1]=='f') flag |= FLAG_FRESH1;
					else if (pchk[1]=='u') flag |= FLAG_BUILD0;
					else if (pchk[1]=='w') flag |= FLAG_DOSAVE;
					else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
				}
				else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
			}
			else fprintf(stderr,"@@ Unknown option '%s'!\n",argv[loop]);
		}
	}
	/* copy path name */
	sprintf(full,"%s/GAME_%d",path,pick);
	printf("@@ Processing %s\n",full);
	/* go to work */
	test = base_load(base,full,size,offs);
	if (test>0) {
		printf("## Base data loaded! (%d)\n",test);
		test = 0;
		for (loop=0;loop<size;loop++) {
			if (base[loop].empty_flag||!base[loop].name[0])
				continue;
			if (!(flag&FLAG_HACKED))
				base_show(&base[loop]);
			if (flag&FLAG_FRESH1)
				pick = base_freshX(&base[loop],BASE_FRESH_BUILDING);
			else if (flag&FLAG_BUILD0) {
				base_build0(&base[loop]);
				pick = 1;
			}
			else pick = 0;
			if (size==1) {
				/* only available for single base mod */
				pick += base_supply_more(&base[loop]);
			}
			if (pick) {
				base_show(&base[loop]);
				test += pick;
			}
		}
		if (test&&(flag&FLAG_DOSAVE)) {
			test = base_save(base,full,size,offs);
			if (test>0) printf("## Changes saved!\n");
			else fprintf(stderr,"** Error saving changes to '%s'!\n",full);
		}
	}
	else fprintf(stderr,"** Error loading data from '%s'!\n",full);
	/* done */
	return 0;
}
/*----------------------------------------------------------------------------*/
