/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define DEFAULT_SAVEPATH "."
#define MAXCHAR_PATHFILE 512
/*----------------------------------------------------------------------------*/
#include "xcom1_locd.h"
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	locd_t locd[LOCD_FULLCOUNT];
	int loop, test, pick, size, filter;
	char *path, *pchk, *ptmp;
	char full[MAXCHAR_PATHFILE];
	char pdef[] = DEFAULT_SAVEPATH;
	path = pdef;
	/* validate! */
	if (sizeof(locd_t)!=LOCD_INFO_SIZE) {
		fprintf(stderr,"** Invalid location data structure! (%d|%d)\n",
			(int)sizeof(locd_t),(int)LOCD_INFO_SIZE);
		return -1;
	}
	/* default setting */
	pick = 1; size = LOCD_FULLCOUNT; filter = 0;
	/* check program args */
	for (loop=1; loop<argc; loop++) {
		pchk = argv[loop];
		if (pchk[0]=='-') {
			if (pchk[1]=='-') {
				ptmp = &pchk[2];
				if (!strcmp(ptmp,"xbase")) filter = LOCD_TYPE_XCOMBASE;
				else if (!strcmp(ptmp,"xcraft")) filter = LOCD_TYPE_XCOMSHIP;
				else if (!strcmp(ptmp,"abase")) filter = LOCD_TYPE_ALIENBASE;
				else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
			}
			else if (!pchk[2]) {
				if (pchk[1]=='g') {
					if (++loop==argc) break;
					pick = atoi(argv[loop]);
					if (pick<0||pick>10) {
						fprintf(stderr,"** Invalid game index (1-10)!\n");
						pick = 1;
					}
				}
				else if (pchk[1]=='p') {
					if (++loop==argc) break;
					path = argv[loop];
				}
				else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
			}
			else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
		}
		else fprintf(stderr,"@@ Unknown parameter '%s'!\n",argv[loop]);
	}
	/* copy path name */
	sprintf(full,"%s/GAME_%d",path,pick);
	printf("@@ Processing %s\n",full);
	/* go to work */
	test = locd_load(locd,full,size,0);
	if (test>0) {
		printf("## Location data loaded! (%d/%d)\n",test,size);
		if (size>test) size = test;
		for (loop=0,test=0;loop<size;loop++) {
			if ((locd[loop].type==LOCD_TYPE_UNUSED)||
					(filter&&locd[loop].type!=filter))
				continue;
			locd_show(&locd[loop]);
		}
/*
			test = locd_save(locd,full,size);
			if (test>0) printf("## Changes saved!\n");
			else fprintf(stderr,"** Error saving changes to '%s'!\n",full);
*/
	}
	else fprintf(stderr,"** Error loading data from '%s'!\n",full);
	/* done */
	return 0;
}
/*----------------------------------------------------------------------------*/
