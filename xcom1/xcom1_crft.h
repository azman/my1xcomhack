/*----------------------------------------------------------------------------*/
#ifndef __MY1XCOM1_CRFT_H__
#define __MY1XCOM1_CRFT_H__
/*----------------------------------------------------------------------------*/
#include "my1type.h"
/*----------------------------------------------------------------------------*/
/**
 * craft data?
 * - purely info from https://www.ufopaedia.org/index.php?title=CRAFT.DAT
 * - do i need this?
**/
/*----------------------------------------------------------------------------*/
typedef struct __crft_t {
	abyte type;
	abyte lweapon;
	aword lw_ammo;
	abyte mode; /* 0:atbase 1:1dest */
	abyte rweapon;
	aword rw_ammo;
	/* offset:0x08 */
	aword unused00;
	aword damage;
	aword altitude;
	aword velocity;
	/* offset:0x10 */
	abyte unknown00[8];
	/* offset:0x18 */
	aword fuel;
	aword base;
	abyte unknown01[14];
	/* offset:0x2a */
	aword status;
	abyte unknown02[60];
} crft_t;
/*----------------------------------------------------------------------------*/
#define CRFT_INFO_SIZE 104
#define CRFT_FULLCOUNT 50
#define CRFT_FULLBYTES (CRFT_INFO_SIZE*CRFT_FULLCOUNT)
#define CRFT_DATA_NAME "CRAFT.DAT"
#define CRFT_DATA_NAMESZ 9
/*----------------------------------------------------------------------------*/
const char XCOM1_CRAFT_NAMES[][32] = {
	{ "Skyranger" },
	{ "Lightning" },
	{ "Avenger" },
	{ "Interceptor" },
	{ "Firestorm" }
};
/*----------------------------------------------------------------------------*/
/* human craft */
#define CRFT_TYPE_SKYRANGER 0x00
#define CRFT_TYPE_LIGHTNING 0x01
#define CRFT_TYPE_AVENGER 0x02
#define CRFT_TYPE_INTERCEPTOR 0x03
#define CRFT_TYPE_FIRESTORM 0x04
/* alien craft */
#define CRFT_TYPE_SMALL_SCOUT 0x05
#define CRFT_TYPE_ALIEN_BEGIN CRFT_TYPE_SMALL_SCOUT
#define CRFT_TYPE_MEDIUM_SCOUT 0x06
#define CRFT_TYPE_LARGE_SCOUT 0x07
#define CRFT_TYPE_HARVESTER 0x08
#define CRFT_TYPE_ABDUCTOR 0x09
#define CRFT_TYPE_TERROR_SHIP 0x0A
#define CRFT_TYPE_BATTLE_SHIP 0x0B
#define CRFT_TYPE_SUPPLY_SHIP 0x0C
/*----------------------------------------------------------------------------*/
#define CRAFT_WEAPON_NONE 0xFF
#define CRAFT_WEAPON_STINGRAY_L 0x00
#define CRAFT_WEAPON_AVALANCE_L 0x01
#define CRAFT_WEAPON_CANON 0x02
#define CRAFT_WEAPON_FUSIONBALL_L 0x03
#define CRAFT_WEAPON_LASER_CANON 0x04
#define CRAFT_WEAPON_PLASMA_BEAM 0x05
/*----------------------------------------------------------------------------*/
#define CRFT_STATUS_READY 0x00
#define CRFT_STATUS_OUT 0x01
#define CRFT_STATUS_REPAIRS 0x02
#define CRFT_STATUS_REFUELING 0x03
#define CRFT_STATUS_REARMING 0x04
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
void crft_show(crft_t* crft) {
	printf("Type: ");
	switch (crft->type) {
		case CRFT_TYPE_SKYRANGER: case CRFT_TYPE_LIGHTNING:
		case CRFT_TYPE_AVENGER: case CRFT_TYPE_INTERCEPTOR:
		case CRFT_TYPE_FIRESTORM:
			printf("%s",XCOM1_CRAFT_NAMES[crft->type]); break;
		default: printf("[%02X]",crft->type); break;
	}
	printf(" @(0x%04x)",crft->base);
	if (crft->damage)
		printf(" (Damage:%d)",crft->damage);
	printf(" Status:%x Fuel:%x\n",crft->status,crft->fuel);
}
/*----------------------------------------------------------------------------*/
int crft_fresh1(crft_t* crft) {
	int stat;
	stat = 0;
	if (crft->status==CRFT_STATUS_REPAIRS||crft->damage) {
		if (crft->damage) {
			printf("-- Repairing (%d>>",crft->damage);
			crft->damage = 0;
			printf("%d) [REPAIRED]\n",crft->damage);
			stat++;
		}
		if (crft->fuel) {
			printf("-- Refueling (%d>>",crft->fuel);
			crft->fuel = 0;
			printf("%d) [FUELLED]\n",crft->fuel);
			stat++;
		}
		/* assuming using plasma beam - no need to rearm! */
		if (crft->status==CRFT_STATUS_REPAIRS) {
			printf("-- Reinstated (%d>>",crft->status);
			crft->status = CRFT_STATUS_READY;
			printf("%d) [READY]\n",crft->status);
			stat++;
		}
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int crft_load(crft_t* crft, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp, full;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += CRFT_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,CRFT_DATA_NAME);
	pdat = fopen(buff,"rb");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk%CRFT_INFO_SIZE||pchk>CRFT_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			full = pchk/CRFT_INFO_SIZE;
			if (offs<full) {
				test = full-offs;
				if (size>test) size = test;
				test = offs*CRFT_INFO_SIZE;
				fseek(pdat,test,SEEK_SET);
				temp = fread((void*)crft,CRFT_INFO_SIZE,size,pdat);
				if (temp!=size) {
					fprintf(stderr,"** Cannot read %s!\n",buff);
					stat = -3;
				}
				else stat = size;
			}
			else stat = -4;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
int crft_save(crft_t* crft, char* path, int size) {
	FILE *pdat;
	char *buff;
	long pchk, temp, full;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += CRFT_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,CRFT_DATA_NAME);
	pdat = fopen(buff,"rb+"); /* w is not ok if 'editing' */
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk%CRFT_INFO_SIZE||pchk>CRFT_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			full = pchk/CRFT_INFO_SIZE;
			if (size>full) size = full;
			fseek(pdat,0,SEEK_SET);
			temp = fwrite((void*)crft,CRFT_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot write %s!\n",buff);
				stat = -3;
			}
			else stat = size;
		}
		fseek(pdat,0,SEEK_END); /* do i need this? */
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1XCOM1_CRFT_H__ */
/*----------------------------------------------------------------------------*/
