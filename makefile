# makefile for my1xcomhack

SOURCES = $(sort $(notdir $(wildcard src/xcom*.c)))
HERESRC = $(sort $(wildcard *.c))
ifneq ($(HERESRC),)
SOURCES += $(HERESRC)
endif
TARGETS = $(subst .c,,$(SOURCES))
X1_SRCS = $(sort $(wildcard xcom1/xcom*.c))
X1_TGTS = $(subst .c,,$(notdir $(X1_SRCS)))
X2_SRCS = $(sort $(wildcard xcom2/xcom*.c))
X2_TGTS = $(subst .c,,$(notdir $(X2_SRCS)))
TARGETS += $(X1_TGTS) $(X2_TGTS)

CC = gcc
DELETE = rm -rf

CFLAGS += -Wall -Iinc/ -Ixcom1/ -Ixcom2/  -Isrc/
LFLAGS = -static
ifneq ($(LDFLAGS),)
LFLAGS += $(LDFLAGS)
endif
OFLAGS =

PHONY_1 = x1merc x1base x1craft x1loc x1hack
PHONY_2 = x2merc x2base x2craft x2loc x2hack

.PHONY: all show clean new xcom1 xcom2 xcom3 $(PHONY_1) $(PHONY_2)

all: $(TARGETS)

show:
	@echo "@@ Targets: { $(TARGETS) }"

xcom1: $(PHONY_1)

xcom2: $(PHONY_2)

xcom3: xcom3hack

x1merc: xcom1_merc

x1base: xcom1_base

x1craft: xcom1_crft

x1loc: xcom1_locd

x1hack: xcom1hack

x2merc: xcom2_merc

x2base: xcom2_base

x2craft: xcom2_crft

x2loc: xcom2_locd

x2hack: xcom2hack

$(TARGETS): % : %.o
	$(CC) -o $@ $< $(LFLAGS) $(OFLAGS)
	strip $@

xcom1hack.o: src/xcom1hack.c $(wildcard xcom1/xcom1_*.h)
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: xcom1/%.c  xcom1/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: xcom2/%.c  xcom2/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: src/%.c  src/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: src/%.c
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: %.c  %.h
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	-$(DELETE) $(TARGETS) *.o

new: clean all
