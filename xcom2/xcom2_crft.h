/*----------------------------------------------------------------------------*/
#ifndef __MY1XCOM2_CRFT_H__
#define __MY1XCOM2_CRFT_H__
/*----------------------------------------------------------------------------*/
#include "my1type.h"
#include "xcom2_path.h"
/*----------------------------------------------------------------------------*/
/**
 * craft data?
 * - purely info from https://www.ufopaedia.org/index.php?title=CRAFT.DAT
 * - do i need this?
**/
/*----------------------------------------------------------------------------*/
typedef struct __crft_t {
	abyte type;
	abyte lweapon;
	aword lw_ammo;
	abyte mode; /* 0:atbase 1:1dest */
	abyte rweapon;
	/* offs:0x06 */
	abyte item_store[56]; /* 0x38 */
	/* offs:0x3e */
	aword rw_ammo;
	/* offs:0x40 */
	aword unused00;
	aword damage;
	aword depth; /* 0-5: touched down, shallow,...,very deep */
	aword velocity;
	/* offs:0x48 */
	aword dest; /* locd index */
	/* offs:0x4a */
	aword unknown00;
	aword dest_h; /* lng */
	aword dest_v; /* lat */
	/* offs:0x50 */
	aword fuel;
	aword base; /* locd index */
	aword mission_type;
	aword mission_zone;
	/* offs:0x58 */
	aword unknown01[2];
	/* offs:0x5c */
	aword primary_race; /* alien race */
	aword attack_timer;
	aword escape_timer;
	/* offs:0x62 */
	aword status;
	abyte flags[4];
	/* offs:0x68 */
	aword touchdown_depth;
	abyte dontcare[4];
	/* size:0x6e @ 110 */
} crft_t;
/*----------------------------------------------------------------------------*/
#define CRFT_INFO_SIZE 110
#define CRFT_FULLCOUNT 50
#define CRFT_FULLBYTES (CRFT_INFO_SIZE*CRFT_FULLCOUNT)
#define CRFT_DATA_NAME "CRAFT.DAT"
#define CRFT_DATA_NAMESZ 9
/*----------------------------------------------------------------------------*/
const char XCOM2_CRAFT_NAMES[][32] = {
	{ "Triton" },
	{ "Hammerhead" },
	{ "Leviathan" },
	{ "Barracuda" },
	{ "Manta" },
	{ "Unknown" }
};
/*----------------------------------------------------------------------------*/
/* human craft */
#define CRFT_TYPE_TRITON 0x00
#define CRFT_TYPE_HAMMERHEAD 0x01
#define CRFT_TYPE_LEVIATHAN 0x02
#define CRFT_TYPE_BARRACUDA 0x03
#define CRFT_TYPE_MANTA 0x04
/* alien craft */
#define CRFT_TYPE_SURVEY_SHIP 0x05
#define CRFT_TYPE_XCOM_UNKNOWN CRFT_TYPE_SURVEY_SHIP
#define CRFT_TYPE_ALIEN_BEGIN CRFT_TYPE_SURVEY_SHIP
#define CRFT_TYPE_ESCORT 0x06
#define CRFT_TYPE_CRUISER 0x07
#define CRFT_TYPE_HEAVY_CRUISER 0x08
#define CRFT_TYPE_HUNTER 0x09
#define CRFT_TYPE_BATTLESHIP 0x0A
#define CRFT_TYPE_DREADNOUGHT 0x0B
#define CRFT_TYPE_FLEET_SUPPLY_CRUISER 0x0C
/*----------------------------------------------------------------------------*/
#define CRAFT_WEAPON_NONE 0xFF
#define CRAFT_WEAPON_AJAX_LAUNCHER 0x00
#define CRAFT_WEAPON_DUP_HEAD_LAUNCHER 0x01
#define CRAFT_WEAPON_CRAFT_GAS_CANNON 0x02
#define CRAFT_WEAPON_PWT_CANNON 0x03
#define CRAFT_WEAPON_GAUSS_CANNON 0x04
#define CRAFT_WEAPON_SONIC_OSCILLATOR 0x05
/*----------------------------------------------------------------------------*/
#define CRFT_STATUS_READY 0x00
#define CRFT_STATUS_OUT 0x01
#define CRFT_STATUS_REPAIRS 0x02
#define CRFT_STATUS_REFUELING 0x03
#define CRFT_STATUS_REARMING 0x04
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
char* crft_type_name(crft_t* crft, char *buff, int size) {
	/* size should be at least 16 bytes? */
	switch (crft->type) {
		case CRFT_TYPE_TRITON: case CRFT_TYPE_HAMMERHEAD:
		case CRFT_TYPE_LEVIATHAN: case CRFT_TYPE_BARRACUDA:
		case CRFT_TYPE_MANTA:
			snprintf(buff,size,"%s",XCOM2_CRAFT_NAMES[crft->type]); break;
		default: snprintf(buff,size,"[%02X]",crft->type); break;
	}
	return buff;
}
/*----------------------------------------------------------------------------*/
#include "xcom2_locd.h"
/*----------------------------------------------------------------------------*/
char* crft_full_name(crft_t* crft, int iref, char *buff, int size) {
	char tbuf[16];
	int test, loop;
	do {
		buff[0] = 0x0;
		if (iref<0||iref>=CRFT_FULLCOUNT) break;
		if (!g_xcom2_path) break;
		locd_load_global(g_xcom2_path);
		if (!g_locd_stat) break;
		for (loop=0,test=-1;loop<LOCD_FULLCOUNT;loop++) {
			if (g_locd[loop].type!=LOCD_TYPE_XCOMSHIP) continue;
			if (g_locd[loop].objr==iref) {
				test = loop; break;
			}
		}
		if (test<0) break;
		snprintf(buff,size,"%s-%d",
			crft_type_name(crft,tbuf,16),(int)g_locd[loop].idx1);
	} while (0);
	return buff;
}
/*----------------------------------------------------------------------------*/
#define crft_show(pc) crft_show_iref(pc,-1)
/*----------------------------------------------------------------------------*/
void crft_show_iref(crft_t* crft, int iref) {
	char buff[16];
	printf("Name: %s",crft_full_name(crft,iref,buff,16));
	printf(" @(0x%04x)",crft->base);
	if (crft->damage)
		printf(" (Damage:%d)",crft->damage);
	printf(" Status:%x Fuel:%x\n",crft->status,crft->fuel);
}
/*----------------------------------------------------------------------------*/
int crft_fresh1(crft_t* crft) {
	int stat;
	stat = 0;
	if (crft->status==CRFT_STATUS_REPAIRS||crft->damage) {
		if (crft->damage) {
			printf("-- Repairing (%d>>",crft->damage);
			crft->damage = 0;
			printf("%d) [REPAIRED]\n",crft->damage);
			stat++;
		}
		if (crft->fuel) {
			printf("-- Refueling (%d>>",crft->fuel);
			crft->fuel = 0;
			printf("%d) [FUELLED]\n",crft->fuel);
			stat++;
		}
		/* assuming using plasma beam - no need to rearm! */
		if (crft->status==CRFT_STATUS_REPAIRS) {
			printf("-- Reinstated (%d>>",crft->status);
			crft->status = CRFT_STATUS_READY;
			printf("%d) [READY]\n",crft->status);
			stat++;
		}
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int crft_load(crft_t* crft, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp, full;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += CRFT_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,CRFT_DATA_NAME);
	pdat = fopen(buff,"rb");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk%CRFT_INFO_SIZE||pchk>CRFT_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			full = pchk/CRFT_INFO_SIZE;
			if (offs<full) {
				test = full-offs;
				if (size>test) size = test;
				test = offs*CRFT_INFO_SIZE;
				fseek(pdat,test,SEEK_SET);
				temp = fread((void*)crft,CRFT_INFO_SIZE,size,pdat);
				if (temp!=size) {
					fprintf(stderr,"** Cannot read %s!\n",buff);
					stat = -3;
				}
				else {
					stat = size;
					g_xcom2_path = path;
				}
			}
			else stat = -4;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
int crft_save(crft_t* crft, char* path, int size) {
	FILE *pdat;
	char *buff;
	long pchk, temp, full;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += CRFT_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,CRFT_DATA_NAME);
	pdat = fopen(buff,"rb+"); /* w is not ok if 'editing' */
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk%CRFT_INFO_SIZE||pchk>CRFT_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			full = pchk/CRFT_INFO_SIZE;
			if (size>full) size = full;
			fseek(pdat,0,SEEK_SET);
			temp = fwrite((void*)crft,CRFT_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot write %s!\n",buff);
				stat = -3;
			}
			else stat = size;
		}
		fseek(pdat,0,SEEK_END); /* do i need this? */
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
int crft_name_index(char* buff, int size, int indx) {
	crft_t crft;
	int test;
	test = crft_load(&crft,g_xcom2_path,1,indx);
	if (test==1) {
		test = snprintf(buff,size,"%s",XCOM2_CRAFT_NAMES[crft.type]);
		if (test<1||test>=size)
			test = 0;
		buff[test] = 0x0;
	}
	else test = 0;
	return test;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1XCOM2_CRFT_H__ */
/*----------------------------------------------------------------------------*/
