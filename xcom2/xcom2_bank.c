/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define DEFAULT_SAVEPATH "."
#define MAXCHAR_PATHFILE 512
/*----------------------------------------------------------------------------*/
#include "xcom2_bank.h"
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	int loop, test, pick, cash;
	char *path, *ptmp, *pchk;
	char full[MAXCHAR_PATHFILE];
	char pdef[] = DEFAULT_SAVEPATH;
	path = pdef;
	/* default setting */
	pick = 1;cash = 0;
	/* check program args */
	if (argc>0) {
		for (loop=1; loop<argc; loop++) {
			pchk = argv[loop];
			if (pchk[0]=='-') {
				if (pchk[1]=='-') {
					ptmp = &pchk[2];
					if (strcmp(ptmp,"cash")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>BANK_FRESH1_CASH)
							fprintf(stderr,"** Invalid size! (%d)\n",test);
						else cash = test;
					}
					else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
				}
				else if (!pchk[2]) {
					if (pchk[1]=='g') {
						if (++loop==argc) break;
						pick = atoi(argv[loop]);
						if (pick<0||pick>10) {
							fprintf(stderr,"** Invalid game index (1-10)!\n");
							pick = 1;
						}
					}
					else if (pchk[1]=='p') {
						if (++loop==argc) break;
						path = argv[loop];
					}
					else if (pchk[1]=='f') cash = BANK_FRESH1_CASH;
					else if (pchk[1]=='u') cash = BANK_BUILD0_CASH;
					else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
				}
				else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
			}
			else fprintf(stderr,"@@ Unknown option '%s'!\n",argv[loop]);
		}
	}
	/* copy path name */
	sprintf(full,"%s/GAME_%d",path,pick);
	printf("@@ Processing %s\n",full);
	/* go to work */
	bank_upgrade(full,cash);
	/* done */
	return 0;
}
/*----------------------------------------------------------------------------*/
