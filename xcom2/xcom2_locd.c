/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define DEFAULT_SAVEPATH "."
#define MAXCHAR_PATHFILE 512
/*----------------------------------------------------------------------------*/
#include "xcom2_locd.h"
#include "xcom2_crft.h"
#include "xcom2_base.h"
/*----------------------------------------------------------------------------*/
void locd_show(locd_t* locd) {
	crft_t crft;
	base_t base;
	int type;
	if (locd->type==LOCD_TYPE_UNUSED) return;
	printf("-- Type: ");
	switch (locd->type) {
		case LOCD_TYPE_XCOMBASE:
			printf("XCOM Base-%d",(int)locd->objr+1);
			/* locd->idx1 something else for XCOM base! */
			if (locd->objr<BASE_FULLCOUNT) {
				if (base_load(&base,g_xcom2_path,1,locd->objr)==1) {
					printf(" (%s)",base.name);
				}
			}
			break;
		case LOCD_TYPE_XCOMSHIP:
			type = CRFT_TYPE_XCOM_UNKNOWN;
			if (crft_load(&crft,g_xcom2_path,1,locd->objr)==1)
				type = crft.type;
			printf("XCOM Craft %s-%d",XCOM2_CRAFT_NAMES[type],locd->idx1);
			break;
		case LOCD_TYPE_ALIENBASE:
			printf("Alien Base-%d ",locd->idx1);
			switch (locd->objr) {
				case LOCD_OBJR_ALIENBASE_AQUATOID:
					printf("{Aquatoid}"); break;
				case LOCD_OBJR_ALIENBASE_GILLMAN:
					printf("{Gillman}"); break;
				case LOCD_OBJR_ALIENBASE_LOBSTERMAN:
					printf("{Lobsterman}"); break;
					break;
				case LOCD_OBJR_ALIENBASE_TASOTH:
					printf("{Tasoth}"); break;
					break;
				case LOCD_OBJR_ALIENBASE_MIXED1:
					printf("{Mixed Crew I}"); break;
					break;
				case LOCD_OBJR_ALIENBASE_MIXED2:
					printf("{Mixed Crew II}"); break;
					break;
				default: printf("{???:%02X}",locd->objr); break;
			}
			break;
		case LOCD_TYPE_ALIENSHIP_MOVING:
			printf("Alien Ship-%d [CRAFT.DAT:%d]",locd->idx1,locd->objr);
			break;
		case LOCD_TYPE_ALIENSHIP_LANDED:
			printf("Alien Ship-%d (Ground) [CRAFT.DAT:%d]",
				locd->idx1,locd->objr);
			break;
		case LOCD_TYPE_CRASHSITE:
			printf("Alien Crash Site-%d (%dH)",locd->idx1,locd->cnt0);
			break;
		case LOCD_TYPE_PORT_ATTACK:
			printf("Port Attack Site-%d (%dH)",locd->idx1,locd->cnt0);
			break;
		case LOCD_TYPE_ISLAND_ATTACK:
			printf("Island Attack Site-%d (%dH)",locd->idx1,locd->cnt0);
			break;
		case LOCD_TYPE_SHIP_ATTACK:
			printf("Ship Attack Site-%d (%dH)",locd->idx1,locd->cnt0);
			break;
		case LOCD_TYPE_WAYPOINT:
			printf("Waypoint-%d",locd->idx1);
			break;
		default: printf("[???:%02X]",locd->type); break;
	}
	printf(" @Pos:(%d,%d)\n",(short int)locd->horz,(short int)locd->vert);
}
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	locd_t locd[LOCD_FULLCOUNT];
	int loop, test, pick, size, filter;
	char *path, *pchk, *ptmp;
	char full[MAXCHAR_PATHFILE];
	char pdef[] = DEFAULT_SAVEPATH;
	path = pdef;
	/* validate! */
	if (sizeof(locd_t)!=LOCD_INFO_SIZE) {
		fprintf(stderr,"** Invalid location data structure! (%d|%d)\n",
			(int)sizeof(locd_t),(int)LOCD_INFO_SIZE);
		return -1;
	}
	/* default setting */
	pick = 1; size = LOCD_FULLCOUNT; filter = 0;
	/* check program args */
	for (loop=1; loop<argc; loop++) {
		pchk = argv[loop];
		if (pchk[0]=='-') {
			if (pchk[1]=='-') {
				ptmp = &pchk[2];
				if (!strcmp(ptmp,"xbase")) filter = LOCD_TYPE_XCOMBASE;
				else if (!strcmp(ptmp,"xcraft")) filter = LOCD_TYPE_XCOMSHIP;
				else if (!strcmp(ptmp,"abase")) filter = LOCD_TYPE_ALIENBASE;
				else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
			}
			else if (!pchk[2]) {
				if (pchk[1]=='g') {
					if (++loop==argc) break;
					pick = atoi(argv[loop]);
					if (pick<0||pick>10) {
						fprintf(stderr,"** Invalid game index (1-10)!\n");
						pick = 1;
					}
				}
				else if (pchk[1]=='p') {
					if (++loop==argc) break;
					path = argv[loop];
				}
				else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
			}
			else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
		}
		else fprintf(stderr,"@@ Unknown parameter '%s'!\n",argv[loop]);
	}
	/* copy path name */
	sprintf(full,"%s/GAME_%d",path,pick);
	printf("@@ Processing %s\n",full);
	/* go to work */
	test = locd_load(locd,full,size,0);
	if (test>0) {
		printf("## Location data loaded! (%d/%d)\n",test,size);
		if (size>test) size = test;
		for (loop=0,test=0;loop<size;loop++) {
			if (locd[loop].type==LOCD_TYPE_UNUSED)
				continue; /* ignore unused */
			test++; /* count used entry */
			if (filter&&(locd[loop].type!=filter))
				continue; /* ignore if not selected */
			locd_show(&locd[loop]);
		}
		printf("## Info: %d location(s)\n",test);
	}
	else fprintf(stderr,"** Error loading data from '%s'!\n",full);
	/* done */
	return 0;
}
/*----------------------------------------------------------------------------*/
