/*----------------------------------------------------------------------------*/
#ifndef __MY1XCOM2_MERC_H__
#define __MY1XCOM2_MERC_H__
/*----------------------------------------------------------------------------*/
#include "my1type.h"
#include "xcom2_path.h"
/*----------------------------------------------------------------------------*/
typedef struct __merc {
	abyte mc_skill_plus, TU_plus, mc_implant_flag, promotion_flag;
	abyte HEA_plus, STR_plus;
	/* offs: 0x06 */
	aword base; /** 0x0000 is first base, refer to LOC.DAT */
	aword craft;
	aword rank;
	aword craft_prev; /** craft before wounded */
	aword kills;
	aword recovery;
	aword missions;
	aword value; /** huh? 0x14? */
	/* offs: 0x16 */
	abyte transfer_flag; /** if base=0xffff, destination base */
	abyte STA_plus, RCT_plus, FA_plus;
	abyte time_unit, health, stamina, throwing_accuracy;
	abyte armor, gender, race, firing_accuracy, strength;
	/* offs: 0x23 */
	abyte name[27]; /** no null needed? only 21 in-game */
	/* offs: 0x3e */
	abyte BRV_plus, TA_plus, bravery;
	abyte mc_skill, MA_plus;
	abyte mc_strength, melee_accuracy, reaction;
	/* size: 0x46 @ 70 */
} merc_t;
/*----------------------------------------------------------------------------*/
#define MERC_INFO_SIZE 70
#define MERC_FULLCOUNT 250
#define MERC_FULLBYTES (MERC_INFO_SIZE*MERC_FULLCOUNT)
#define MERC_DATA_NAME "SOLDIER.DAT"
#define MERC_DATA_NAMESZ 11
/*----------------------------------------------------------------------------*/
#define MERC_BRAVERY(x) (int)((110-x)/10)
/*----------------------------------------------------------------------------*/
#define MERC_RANK_INVALID 0xFFFF
#define MERC_RANK_SEAMAN 0x0000
#define MERC_RANK_ABLESEAMAN 0x0001
#define MERC_RANK_ENSIGN 0x0002
#define MERC_RANK_LIEUTENANT 0x0003
#define MERC_RANK_COMMANDER 0x0004
#define MERC_RANK_CAPTAIN 0x0005
#define MERC_ARMOR_NONE 0x00
#define MERC_ARMOR_PLASTIC_AQUA 0x01
#define MERC_ARMOR_ION_ARMOR 0x02
#define MERC_ARMOR_MAGNETIC_ION_ARMOR 0x03
/*----------------------------------------------------------------------------*/
#define MERC_SEAMAN MERC_RANK_SEAMAN
#define MERC_ABLESEAMAN MERC_RANK_ABLESEAMAN
#define MERC_ENSIGN MERC_RANK_ENSIGN
#define MERC_LIEUTENANT MERC_RANK_LIEUTENANT
#define MERC_COMMANDER MERC_RANK_COMMANDER
#define MERC_CAPTAIN MERC_RANK_CAPTAIN
/*----------------------------------------------------------------------------*/
#define MERC_STAT_FULL 0x64
#define MERC_PLUS_FULL 0x32
/*----------------------------------------------------------------------------*/
#define MERC_NONE MERC_RANK_INVALID
#define MERC_SUPER0_NAME "Maverick"
#define MERC_BRAVE_X(x) (int)(x/10)
#define MERC_VALUE_ROOKIE 0x14
/*----------------------------------------------------------------------------*/
#define MERC(pm) ((merc_t*)pm)
#define merc_null(pm) (MERC(pm)->rank==MERC_NONE)
#define merc_name(pm) (MERC(pm)->name[0])
#define merc_duty(pm) (MERC(pm)->missions)
#define merc_xval(pm) ((int)(MERC(pm)->value)>=MERC_VALUE_ROOKIE)
#define merc_died(pm) (merc_name(pm)&&merc_duty(pm)&&merc_xval(pm))
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#define merc_show(pm) merc_show_core(MERC(pm),"-- Soldier: ")
/*----------------------------------------------------------------------------*/
void merc_show_info(merc_t* merc) {
	if (merc->name[0])
		printf("%s ",merc->name);
	printf("(");
	if (!merc_null(merc)&&!(merc->gender&0xfe))
		printf("%c:",(merc->gender?'F':'M'));
	else printf("0x%02x:",merc->gender);
	switch (merc->rank) {
		case MERC_SEAMAN: printf("Seaman"); break;
		case MERC_ABLESEAMAN: printf("Able Seaman"); break;
		case MERC_ENSIGN: printf("Ensign"); break;
		case MERC_LIEUTENANT: printf("Lieutenant"); break;
		case MERC_COMMANDER: printf("Commander"); break;
		case MERC_CAPTAIN: printf("Captain"); break;
		default: printf("0x%04X",merc->rank); break;
	}
	printf(")");
}
/*----------------------------------------------------------------------------*/
#include "xcom2_locd.h"
#include "xcom2_base.h"
#include "xcom2_crft.h"
/*----------------------------------------------------------------------------*/
void merc_show_locd(merc_t* merc) {
	char buff[16];
	int test, temp;
	buff[0] = 0x0;
	if (!merc_null(merc)) {
		temp = merc->base;
		if (temp==0xffff)
			temp = merc->transfer_flag;
		test = locd_base_index(temp);
		if (test>=0) {
			base_name_index(buff,16,test);
			if (temp!=merc->base)
				strcat(buff,"*");
		}
	}
	if (!buff[0]) snprintf(buff,16,"0x%04X",merc->base);
	printf(" Base:{%s}",buff);
	if (merc->craft!=0xffff) {
		buff[0] = 0x0;
		if (!merc_null(merc)) {
			test = locd_ship_index(merc->craft);
			if (test>=0) {
				temp = crft_name_index(buff,16,test);
				if (temp) {
					snprintf(&buff[temp],16-temp,"-%d",
						locd_iref_step(merc->craft));
				}
			}
		}
		if (!buff[0]) snprintf(buff,16,"0x%04X",merc->craft);
		printf(" Craft:{%s}",buff);
	}
	if (merc->craft_prev!=0xffff) {
		buff[0] = 0x0;
		if (!merc_null(merc)) {
			test = locd_ship_index(merc->craft_prev);
			if (test>=0) {
				temp = crft_name_index(buff,16,test);
				if (temp) {
					snprintf(&buff[temp],16-temp,"-%d",
						locd_iref_step(merc->craft_prev));
				}
			}
		}
		if (!buff[0]) snprintf(buff,16,"0x%04X",merc->craft_prev);
		printf(" CraftPrev:{%s}",buff);
	}
}
/*----------------------------------------------------------------------------*/
void merc_show_face(merc_t* merc) {
	printf(" [R:0x%02x]\n",merc->race);
}
/*----------------------------------------------------------------------------*/
void merc_show_init(merc_t* merc, char* pstr) {
	if (pstr) printf(pstr);
	merc_show_info(merc);
	merc_show_locd(merc);
	merc_show_face(merc);
}
/*----------------------------------------------------------------------------*/
void merc_show_stat(merc_t* merc) {
	if (merc->recovery)
		printf("   ** Wounded:%d day(s) to recover\n",merc->recovery);
	if (merc->mc_implant_flag)
		printf("   @@ MC Implant:%d\n",merc->mc_implant_flag);
	printf("   > Mission(s):%d, Kill(s):%d, Value:%d\n",
		merc->missions,merc->kills,merc->value);
	printf("   > TU:%d(+%d), Strength:%d(+%d), Stamina:%d(+%d)\n",
		merc->time_unit,merc->TU_plus,merc->strength,merc->STR_plus,
		merc->stamina,merc->STA_plus);
	printf("   > Health:%d(+%d), Reaction:%d(+%d), Bravery:%d(+%d)\n",
		merc->health,merc->HEA_plus,merc->reaction,merc->RCT_plus,
		merc->bravery,merc->BRV_plus);
	printf("   > Firing Acc.:%d(+%d), Throwing Acc.:%d(+%d)\n",
		merc->firing_accuracy,merc->FA_plus,
		merc->throwing_accuracy,merc->TA_plus);
	printf("   > Melee Acc.:%d(+%d), MC Skill:%d(+%d), MC Strength:%d\n",
		merc->melee_accuracy,merc->MA_plus,
		merc->mc_skill,merc->mc_skill_plus,merc->mc_strength);
}
/*----------------------------------------------------------------------------*/
void merc_show_core(merc_t* merc, char* pstr) {
	merc_show_init(merc,pstr);
	merc_show_stat(merc);
}
/*----------------------------------------------------------------------------*/
int merc_build0(merc_t* merc) {
	int stat = 0;
	if (merc->rank==MERC_NONE) return stat;
	if (merc->time_unit<MERC_STAT_FULL) {
		merc->time_unit = MERC_STAT_FULL;
		stat++;
	}
	if (merc->health<MERC_STAT_FULL) {
		merc->health = MERC_STAT_FULL;
		stat++;
	}
	if (merc->stamina<MERC_STAT_FULL) {
		merc->stamina = MERC_STAT_FULL;
		stat++;
	}
	if (merc->reaction<MERC_STAT_FULL) {
		merc->reaction = MERC_STAT_FULL;
		stat++;
	}
	if (merc->strength<MERC_STAT_FULL) {
		merc->strength = MERC_STAT_FULL;
		stat++;
	}
	if (merc->firing_accuracy<MERC_STAT_FULL) {
		merc->firing_accuracy = MERC_STAT_FULL;
		stat++;
	}
	if (merc->throwing_accuracy<MERC_STAT_FULL) {
		merc->throwing_accuracy = MERC_STAT_FULL;
		stat++;
	}
	if (merc->melee_accuracy<MERC_STAT_FULL) {
		merc->melee_accuracy = MERC_STAT_FULL;
		stat++;
	}

	if (merc->mc_strength<MERC_STAT_FULL) {
		merc->mc_strength = MERC_STAT_FULL;
		stat++;
	}
	if (merc->mc_skill<MERC_STAT_FULL) {
		merc->mc_skill = MERC_STAT_FULL;
		stat++;
	}
	if (merc->bravery!=MERC_BRAVERY(100)) {
		merc->bravery = MERC_BRAVERY(100);
		stat++;
	}
	if (merc->armor!=MERC_ARMOR_MAGNETIC_ION_ARMOR) {
		merc->armor = MERC_ARMOR_MAGNETIC_ION_ARMOR;
		stat++;
	}
	if (stat) printf("-- Full stat(s) for %s\n",(char*)merc->name);
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_buildX(merc_t* merc) {
	int stat = 0;
	if (merc->rank==MERC_NONE) return stat;
	if (merc->TU_plus<MERC_PLUS_FULL) {
		merc->TU_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->HEA_plus<MERC_PLUS_FULL) {
		merc->HEA_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->STA_plus<MERC_PLUS_FULL) {
		merc->STA_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->RCT_plus<MERC_PLUS_FULL) {
		merc->RCT_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->STR_plus<MERC_PLUS_FULL) {
		merc->STR_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->FA_plus<MERC_PLUS_FULL) {
		merc->FA_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->TA_plus<MERC_PLUS_FULL) {
		merc->TA_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->MA_plus<MERC_PLUS_FULL) {
		merc->MA_plus = MERC_PLUS_FULL;
		stat++;
	}
	if (merc->BRV_plus<MERC_BRAVE_X(MERC_PLUS_FULL)) {
		merc->BRV_plus = MERC_BRAVE_X(MERC_PLUS_FULL);
		stat++;
	}
	if (stat) printf("-- PLUS stat(s) for %s\n",(char*)merc->name);
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_revive(merc_t* merc) {
	int stat = 0;
	if (merc_died(merc)) {
		printf("-- Resurrecting %s as seaman!\n",(char*)merc->name);
		merc->rank = MERC_SEAMAN;
		merc->value = MERC_VALUE_ROOKIE;
		stat++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_fresh1(merc_t* merc) {
	int stat = 0;
	if (merc->rank==MERC_NONE) {
		if (merc_revive(merc)) stat++;
		else return stat;
	}
	stat += merc_build0(merc);
	if (merc->recovery) {
		merc->recovery = 0;
		printf("-- Full recovery for wounded %s!\n",(char*)merc->name);
		merc->craft = merc->craft_prev;
		merc->craft_prev = 0xffff;
		stat++;
	}
	if (merc->bravery!=MERC_BRAVERY(MERC_STAT_FULL)) {
		merc->bravery = MERC_BRAVERY(MERC_STAT_FULL);
		printf("-- Restoring bravery for %s!\n",(char*)merc->name);
		stat++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_domake(merc_t* merc, char *name) {
	char buff[] = MERC_SUPER0_NAME;
	int stat = 0;
	/* works on 'empty' slot */
	if (merc->rank==MERC_NONE) {
		merc->rank = MERC_SEAMAN;
		if (!name) name = buff;
		strcpy((char*)merc->name,MERC_SUPER0_NAME);
		merc->value = MERC_VALUE_ROOKIE;
		merc->craft = 0xFFFF; /* new hires gets no craft assignment */
		stat++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_super0(merc_t* merc) {
	int stat;
	stat = merc_domake(merc,MERC_SUPER0_NAME);
	if (stat) printf("-- Training ");
	else printf("-- Upgrading ");
	printf("%s as super merc!\n",(char*)merc->name);
	stat += merc_build0(merc);
	stat += merc_buildX(merc);
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_load(merc_t* merc, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += MERC_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,MERC_DATA_NAME);
	pdat = fopen(buff,"rb");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk!=MERC_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			test = MERC_FULLCOUNT-offs;
			if (size>test) size = test;
			test = offs*MERC_INFO_SIZE;
			fseek(pdat,test,SEEK_SET);
			temp = fread((void*)merc,MERC_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot read %s!\n",buff);
				stat = -3;
			}
			else {
				stat = size;
				g_xcom2_path = path;
			}
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_save(merc_t* merc, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += MERC_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,MERC_DATA_NAME);
	pdat = fopen(buff,"rb+"); /* w is not ok if 'editing' */
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk!=MERC_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			test = MERC_FULLCOUNT-offs;
			if (size>test) size = test;
			test = offs*MERC_INFO_SIZE;
			fseek(pdat,test,SEEK_SET);
			temp = fwrite((void*)merc,MERC_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot write %s!\n",buff);
				stat = -3;
			}
			else stat = size;
		}
		fseek(pdat,0,SEEK_END); /* do i need this? */
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1XCOM2_MERC_H__ */
/*----------------------------------------------------------------------------*/
