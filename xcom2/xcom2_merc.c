/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define DEFAULT_SAVEPATH "."
#define MAXCHAR_PATHFILE 512
/*----------------------------------------------------------------------------*/
#define FLAG_MODDED 0x8000
#define FLAG_DOFULL 0x0100
#define FLAG_DOSAVE 0x0080
#define FLAG_CREATE 0x0010
#define FLAG_SUPER0 0x0008
#define FLAG_BUILD0 0x0004
#define FLAG_BUILDX 0x0002
#define FLAG_FRESH1 0x0001
#define FLAG_EXTRA0 (FLAG_BUILD0|FLAG_BUILDX)
#define FLAG_MAKE00 (FLAG_CREATE|FLAG_BUILD0)
#define FLAG_MODREQ (FLAG_SUPER0|FLAG_EXTRA0|FLAG_FRESH1|FLAG_CREATE)
/*----------------------------------------------------------------------------*/
#define modified(pf) (pf&FLAG_MODDED)
#define modify(pf) pf|=FLAG_MODDED
/*----------------------------------------------------------------------------*/
#define save_req(pf) (pf&FLAG_DOSAVE)
#define do_save(pf) pf|=FLAG_DOSAVE
/*----------------------------------------------------------------------------*/
#define super_req(pf) (pf&FLAG_SUPER0)
#define do_super(pf) pf|=FLAG_SUPER0
#define build_req(pf) (pf&FLAG_BUILD0)
#define do_build(pf) pf|=FLAG_BUILD0
#define extra_req(pf) (pf&FLAG_BUILDX)
#define do_extra(pf) pf|=FLAG_BUILDX
#define fresh_req(pf) (pf&FLAG_FRESH1)
#define do_fresh(pf) pf|=FLAG_FRESH1
/*----------------------------------------------------------------------------*/
#define create_req(pf) (pf&FLAG_CREATE)
#define modify_req(pf) (pf&FLAG_MODREQ)
/*----------------------------------------------------------------------------*/
#define do_create(pf) pf|=FLAG_MAKE00
#define do_upgrade(pf) pf|=FLAG_EXTRA0
/*----------------------------------------------------------------------------*/
#include "xcom2_merc.h"
#include "xcom2_base.h"
#include "xcom2_crft.h"
#include "xcom2_locd.h"
/*----------------------------------------------------------------------------*/
#define MERC_LOAD_DEFAULT 10
/*----------------------------------------------------------------------------*/
int merc_base_name(merc_t* merc, char* path, char *name, int size) {
	base_t base;
	locd_t locd;
	int stat, test;
	stat = 0;
	do {
		test = locd_load(&locd,path,1,merc->base);
		if (test!=1) break;
		/* must be xcom base! */
		if (locd.type!=LOCD_TYPE_XCOMBASE) break;
		/* get base info in BASE.DAT */
		if (locd.objr<BASE_FULLCOUNT) {
			if (base_load(&base,path,1,locd.objr)==1) {
				snprintf(name,size-1,"%s",base.name);
				name[size-1] = 0x0; /* just in case */
				stat++;
			}
		}
	} while (0);
	return stat;
}
/*----------------------------------------------------------------------------*/
int merc_crft_name(merc_t* merc, char* path, char* name, int size) {
	crft_t crft;
	locd_t locd;
	int stat, test;
	stat = 0;
	do {
		test = locd_load(&locd,path,1,merc->craft);
		if (test!=1) break;
		/* must be xcom craft! */
		if (locd.type!=LOCD_TYPE_XCOMSHIP) break;
		/* get craft info in CRAFT.DAT */
		if (crft_load(&crft,path,1,locd.objr)==1) {
			snprintf(name,size-1,"%s-%d",
				XCOM2_CRAFT_NAMES[crft.type],locd.idx1);
			name[size-1] = 0x0; /* just in case */
			stat++;
		}
	} while (0);
	return stat;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	merc_t merc[MERC_FULLCOUNT];
	int loop, test, pick, offs, size, flag, scnt;
	char *path, *ptmp, *pchk;
	char buff[32];
	char full[MAXCHAR_PATHFILE];
	char pdef[] = DEFAULT_SAVEPATH;
	path = pdef;
	/* validate! */
	if (sizeof(merc_t)!=MERC_INFO_SIZE) {
		fprintf(stderr,"** Invalid soldier data structure! (%d|%d)\n",
			(int)sizeof(merc_t),(int)MERC_INFO_SIZE);
		return -1;
	}
	/* default setting */
	pick = 1; offs = 0; size = MERC_LOAD_DEFAULT;
	flag = 0; scnt = 0;
	/* check program args */
	for (loop=1; loop<argc; loop++) {
		pchk = argv[loop];
		if (pchk[0]=='-') {
			if (pchk[1]=='-') {
				ptmp = &pchk[2];
				if (strcmp(ptmp,"offs")==0) {
					if (++loop==argc) break;
					test = atoi(argv[loop]);
					if (test<0||test>=MERC_FULLCOUNT)
						fprintf(stderr,"** Invalid offset!\n");
					else offs = test;
				}
				else if (strcmp(ptmp,"size")==0) {
					if (++loop==argc) break;
					test = atoi(argv[loop]);
					if (test<1||test>(MERC_FULLCOUNT-offs))
						fprintf(stderr,"** Invalid size!\n");
					else size = test;
				}
				else if (strcmp(ptmp,"all")==0) {
					offs = 0;
					size = MERC_FULLCOUNT;
				}
				else if (strcmp(ptmp,"full")==0) flag |= FLAG_DOFULL;
				else if (strcmp(ptmp,"make")==0) do_create(flag);
				else if (strcmp(ptmp,"super")==0) do_super(flag);
				else if (strcmp(ptmp,"extra")==0) do_upgrade(flag);
				else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
			}
			else if (!pchk[2]) {
				if (pchk[1]=='g') {
					if (++loop==argc) break;
					pick = atoi(argv[loop]);
					if (pick<0||pick>10) {
						fprintf(stderr,"** Invalid game index (1-10)!\n");
						pick = 1;
					}
				}
				else if (pchk[1]=='p') {
					if (++loop==argc) break;
					path = argv[loop];
				}
				else if (pchk[1]=='i') {
					if (++loop==argc) break;
					test = atoi(argv[loop]);
					if (test<1||test>MERC_FULLCOUNT) {
						fprintf(stderr,"** Invalid merc index (1-%d)!\n",
							MERC_FULLCOUNT);
					}
					else {
						offs = test-1;
						size = 1;
					}
				}
				else if (pchk[1]=='f') flag |= FLAG_FRESH1;
				else if (pchk[1]=='u') flag |= FLAG_BUILD0;
				else if (pchk[1]=='w') flag |= FLAG_DOSAVE;
				else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
			}
			else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
		}
		else fprintf(stderr,"@@ Unknown parameter '%s'!\n",argv[loop]);
	}
	/* copy path name */
	sprintf(full,"%s/GAME_%d",path,pick);
	printf("@@ Processing %s\n",full);
	/* go to work */
	test = merc_load(merc,full,size,offs);
	if (test>0) {
		for (loop=0,scnt=0;loop<size;loop++) {
			if (!merc_null(&merc[loop]))
				scnt++;
		}
		printf("## Merc data loaded! (%d)\n",scnt);
		test = 0;
		for (loop=0;loop<size;loop++) {
			sprintf(buff,"[%02d] ",offs+loop);
			if (!modify_req(flag)) {
				if (size==MERC_FULLCOUNT&&merc_null(&merc[loop]))
					continue;
				merc_show_init(&merc[loop],buff);
				if ((flag&FLAG_DOFULL)&&!merc_null(&merc[loop]))
					merc_show_stat(&merc[loop]);
			}
			if (super_req(flag)) /* super overrides all */
				pick = merc_super0(&merc[loop]);
			else {
				if (merc_null(&merc[loop])) {
					if (merc_revive(&merc[loop]))
						pick = 1;
					else if (create_req(flag)) {
						/* should allow new name here! */
						pick = merc_domake(&merc[loop],MERC_SUPER0_NAME);
						if (pick)
							printf("-- Training new soldier: %s\n",
								(char*)merc[loop].name);
					}
					else continue;
				}
				else pick = 0;
				if (build_req(flag))
					pick += merc_build0(&merc[loop]);
				if (extra_req(flag))
					pick += merc_buildX(&merc[loop]);
				if (fresh_req(flag))
					pick += merc_fresh1(&merc[loop]);
			}
			if (pick) {
				merc_show_init(&merc[loop],buff);
				merc_show_stat(&merc[loop]);
				test += pick;
			}
		}
		if (test&&save_req(flag)) {
			test = merc_save(merc,full,size,offs);
			if (test>0) printf("## Changes saved!\n");
			else fprintf(stderr,"** Error saving changes to '%s'!\n",full);
		}
	}
	else fprintf(stderr,"** Error loading data from '%s'!\n",full);
	/* done */
	return 0;
}
/*----------------------------------------------------------------------------*/
