/*----------------------------------------------------------------------------*/
#ifndef __MY1XCOM2_BANK_H__
#define __MY1XCOM2_BANK_H__
/*----------------------------------------------------------------------------*/
#include "my1type.h"
/*----------------------------------------------------------------------------*/
#define BANK_DATA_NAME "LIGLOB.DAT"
#define BANK_DATA_NAMESZ 13
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
/* this is ~$67mil */
#define BANK_BUILD0_CASH 0x03ffffff
/* this is ~$16mil */
#define BANK_FRESH1_CASH 0x00ffffff
#define BANK_MIN_CASH 1000000
/*----------------------------------------------------------------------------*/
int bank_upgrade(char* path, int cash) {
	FILE *pdat;
	char *buff;
	long pchk, temp;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += BANK_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,BANK_DATA_NAME);
	pdat = fopen(buff,"rb+");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		fprintf(stderr,"@@ Size{%s}:%d\n",buff,(int)pchk);
		fseek(pdat,0,SEEK_SET);
		temp = fread((void*)&test,sizeof(int),1,pdat);
		if (temp!=1) {
			fprintf(stderr,"** Cannot read %s!\n",buff);
			stat = -3;
		}
		else {
			fprintf(stderr,"@@ CashCurrent:$%d (%x)\n",test,test);
			if (test<BANK_MIN_CASH) {
				if (cash<BANK_FRESH1_CASH)
					cash += BANK_FRESH1_CASH;
			}
			if (cash>0&&cash>test) {
				fseek(pdat,0,SEEK_SET);
				temp = fwrite((void*)&cash,sizeof(int),1,pdat);
				if (temp!=1) {
					fprintf(stderr,"** Cannot add cash to %s!\n",buff);
					stat = -4;
				}
				else {
					fprintf(stderr,"## CashProvided:$%d\n",cash);
					stat = cash;
				}
			}
			else stat = test;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1XCOM2_BANK_H__ */
/*----------------------------------------------------------------------------*/
