/*----------------------------------------------------------------------------*/
#ifndef __MY1XCOM2_LOCD_H__
#define __MY1XCOM2_LOCD_H__
/*----------------------------------------------------------------------------*/
#include "my1type.h"
#include "xcom2_path.h"
/*----------------------------------------------------------------------------*/
/**
 * location/container data?
 * - purely info from https://www.ufopaedia.org/index.php?title=LOC.DAT
**/
/*----------------------------------------------------------------------------*/
typedef struct __locd_t {
	abyte type;
	abyte objr; /* object refs: e.g. ship(CRAFT.DAT) xcombase(BASE.DAT) */
	aword horz; /* horizontal coord: 0 to 2880 */
	aword vert; /* vertical coord: -720 to 720 */
	/**
	 * count down timer
	 * - crash/terror site: hours exists?
	 * - moving object: game-ticks (5s) to target (cellsize/speed)
	**/
	aword cnt0;
	/* fraction of cnt0 for moving object (cellsize%speed) */
	aword cnt1;
	aword idx1; /* base-1 index for item */
	abyte notused00[2];
	abyte tmod; /* transfer mode 0x01:in transfer 0x02:transfer select bit */
	abyte notused01;
	abyte unknown00[4]; /* visibility@mobility bit fields? */
} locd_t;
/*----------------------------------------------------------------------------*/
#define LOCD_INFO_SIZE 20
#define LOCD_FULLCOUNT 50
#define LOCD_FULLBYTES (LOCD_INFO_SIZE*LOCD_FULLCOUNT)
#define LOCD_DATA_NAME "LOC.DAT"
#define LOCD_DATA_NAMESZ 7
/*----------------------------------------------------------------------------*/
#define LOCD_TYPE_UNUSED 0x00
#define LOCD_TYPE_ALIENSHIP_MOVING 0x01
#define LOCD_TYPE_XCOMSHIP 0x02
#define LOCD_TYPE_XCOMBASE 0x03
#define LOCD_TYPE_ALIENBASE 0x04
#define LOCD_TYPE_CRASHSITE 0x05
#define LOCD_TYPE_ALIENSHIP_LANDED 0x06
#define LOCD_TYPE_WAYPOINT 0x07
#define LOCD_TYPE_PORT_ATTACK 0x51
#define LOCD_TYPE_ISLAND_ATTACK 0x52
#define LOCD_TYPE_SHIP_ATTACK 0x53
#define LOCD_TYPE_ARTIFACT_SITE 0x54
/*----------------------------------------------------------------------------*/
#define LOCD_OBJR_ALIENBASE_AQUATOID 0x00
#define LOCD_OBJR_ALIENBASE_GILLMAN 0x01
#define LOCD_OBJR_ALIENBASE_LOBSTERMAN 0x02
#define LOCD_OBJR_ALIENBASE_TASOTH 0x03
#define LOCD_OBJR_ALIENBASE_MIXED1 0x04
#define LOCD_OBJR_ALIENBASE_MIXED2 0x05
/*----------------------------------------------------------------------------*/
int locd_load(locd_t* locd, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp, full;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += LOCD_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,LOCD_DATA_NAME);
	pdat = fopen(buff,"rb");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk%LOCD_INFO_SIZE||pchk>LOCD_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			full = pchk/LOCD_INFO_SIZE;
			if (offs<full) {
				test = full-offs;
				if (size>test) size = test;
				test = offs*LOCD_INFO_SIZE;
				fseek(pdat,test,SEEK_SET);
				temp = fread((void*)locd,LOCD_INFO_SIZE,size,pdat);
				if (temp!=size) {
					fprintf(stderr,"** Cannot read %s!\n",buff);
					stat = -3;
				}
				else {
					stat = size;
					g_xcom2_path = path; /* only on success */
				}
			}
			else stat = -4;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
static locd_t g_locd[LOCD_FULLCOUNT];
static int g_locd_stat;
/*----------------------------------------------------------------------------*/
int locd_load_global(char* path) {
	g_locd_stat = locd_load(g_locd,path,LOCD_FULLCOUNT,0);
	if (g_locd_stat!=LOCD_FULLCOUNT) g_locd_stat = 0;
	return g_locd_stat;
}
/*----------------------------------------------------------------------------*/
int locd_type_index(int type, int lref) {
	int stat = -1;
	do {
		if (!g_locd_stat) {
			if (!g_xcom2_path) break;
			stat--;
			locd_load_global(g_xcom2_path);
			if (!g_locd_stat) break;
			stat--;
		}
		if (g_locd[lref].type==type)
			stat = g_locd[lref].objr;
	} while (0);
	return stat;
}
/*----------------------------------------------------------------------------*/
int locd_iref_step(int lref) {
	int stat = -1;
	do {
		if (!g_locd_stat) {
			if (!g_xcom2_path) break;
			stat--;
			locd_load_global(g_xcom2_path);
			if (!g_locd_stat) break;
			stat--;
		}
		stat = g_locd[lref].idx1;
	} while (0);
	return stat;
}
/*----------------------------------------------------------------------------*/
#define locd_base_index(pr) locd_type_index(LOCD_TYPE_XCOMBASE,pr)
#define locd_ship_index(pr) locd_type_index(LOCD_TYPE_XCOMSHIP,pr)
/*----------------------------------------------------------------------------*/
/* maybe do not need this? */
/*----------------------------------------------------------------------------*/
int locd_save(locd_t* locd, char* path, int size) {
	FILE *pdat;
	char *buff;
	long pchk, temp, full;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += LOCD_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,LOCD_DATA_NAME);
	pdat = fopen(buff,"rb+"); /* w is not ok if 'editing' */
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk%LOCD_INFO_SIZE||pchk>LOCD_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			full = pchk/LOCD_INFO_SIZE;
			if (size>full) size = full;
			fseek(pdat,0,SEEK_SET);
			temp = fwrite((void*)locd,LOCD_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot write %s!\n",buff);
				stat = -3;
			}
			else stat = size;
		}
		fseek(pdat,0,SEEK_END); /* do i need this? */
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1XCOM2_LOCD_H__ */
/*----------------------------------------------------------------------------*/
