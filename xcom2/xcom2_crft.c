/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define DEFAULT_SAVEPATH "."
#define MAXCHAR_PATHFILE 512
/*----------------------------------------------------------------------------*/
#include "xcom2_crft.h"
/*----------------------------------------------------------------------------*/
#define FLAG_DOSAVE 0x8000
#define FLAG_DOHACK 0x0001
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	crft_t crft[CRFT_FULLCOUNT];
	int loop, test, pick, size, flag;
	char *path, *pchk;
	char full[MAXCHAR_PATHFILE];
	char pdef[] = DEFAULT_SAVEPATH;
	path = pdef;
	/* validate! */
	if (sizeof(crft_t)!=CRFT_INFO_SIZE) {
		fprintf(stderr,"** Invalid craft data structure! (%d|%d)\n",
			(int)sizeof(crft_t),(int)CRFT_INFO_SIZE);
		return -1;
	}
	/* default setting */
	pick = 1; size = CRFT_FULLCOUNT; flag = 0;
	/* check program args */
	for (loop=1; loop<argc; loop++) {
		pchk = argv[loop];
		if (pchk[0]=='-') {
			if (!pchk[2]) {
				if (pchk[1]=='g') {
					if (++loop==argc) break;
					pick = atoi(argv[loop]);
					if (pick<0||pick>10) {
						fprintf(stderr,"** Invalid game index (1-10)!\n");
						pick = 1;
					}
				}
				else if (pchk[1]=='p') {
					if (++loop==argc) break;
					path = argv[loop];
				}
				else if (pchk[1]=='f') flag |= FLAG_DOHACK;
				else if (pchk[1]=='w') flag |= FLAG_DOSAVE;
				else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
			}
			else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
		}
		else fprintf(stderr,"@@ Unknown parameter '%s'!\n",argv[loop]);
	}
	/* copy path name */
	sprintf(full,"%s/GAME_%d",path,pick);
	printf("@@ Processing %s\n",full);
	/* go to work */
	test = crft_load(crft,full,size,0);
	if (test>0) {
		printf("## Craft data loaded! (%d/%d)\n",test,size);
		if (size>test) size = test;
		for (loop=0,test=0;loop<size;loop++) {
			if (crft[loop].type>=CRFT_TYPE_ALIEN_BEGIN) continue;
			if (!(flag&FLAG_DOHACK)) {
				printf("[%02d] ",loop);
				crft_show_iref(&crft[loop],loop);
				continue;
			}
			pick = crft_fresh1(&crft[loop]);
			if (pick) {
				printf(">> [%02d] ",loop);
				crft_show_iref(&crft[loop],loop);
				test += pick;
			}
		}
		if (test&&(flag&FLAG_DOSAVE)) {
			test = crft_save(crft,full,size);
			if (test>0) printf("## Changes saved!\n");
			else fprintf(stderr,"** Error saving changes to '%s'!\n",full);
		}


	}
	else fprintf(stderr,"** Error loading data from '%s'!\n",full);
	/* done */
	return 0;
}
/*----------------------------------------------------------------------------*/
