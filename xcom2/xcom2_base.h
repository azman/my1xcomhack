/*----------------------------------------------------------------------------*/
#ifndef __MY1XCOM2_BASE_H__
#define __MY1XCOM2_BASE_H__
/*----------------------------------------------------------------------------*/
#include "my1type.h"
#include "xcom2_path.h"
/*----------------------------------------------------------------------------*/
#define BASE_NAME_SIZE 14
#define BASE_TILE_ROWS 6
#define BASE_TILE_COLS 6
#define BASE_TILE_COUNT (BASE_TILE_ROWS*BASE_TILE_COLS)
/*----------------------------------------------------------------------------*/
typedef struct __base_t {
	abyte name[BASE_NAME_SIZE];
	aword name_sep; /** null */
	/* cws = craft weapon system, cwa = craft weapon ammo */
	aword cws_ajax_launcher;
	aword cws_dup_head_launcher;
	aword cws_craft_gas_cannon;
	aword cws_pwt_cannon;
	aword cws_gauss_cannon;
	aword cws_sonic_oscillator;
	aword cwa_ajax_torpedos;
	aword cwa_dup_head_torpedos;
	aword cwa_gas_rounds_x50;
	aword cwa_pwt_ammo;
	/* avs = assault vehicle system, ava = assault vehicle ammo */
	aword avs_coelacanth_gcannon;
	aword avs_coelacanth_aquajet;
	aword avs_coelacanth_gauss;
	aword avs_displacer_sonic;
	aword avs_displacer_pwt;
	/* pwp = personal weapon, pwa = personal weapon ammo */
	aword pwp_dart_gun;
	aword pwa_dart_clip;
	aword pwp_jet_harpoon;
	aword pwa_harpoon_clip;
	aword pwp_gas_cannon;
	aword pwa_gc_ap_bolts;
	aword pwa_gc_he_bolts;
	aword pwa_gc_phosphorus_bolts;
	aword pwp_hydro_jet_cannon;
	aword pwa_hj_ap_ammo;
	aword pwa_hj_he_ammo;
	aword pwa_hj_phosphorus_ammo;
	aword pwp_torpedo_launcher;
	aword pwa_small_torpedo;
	aword pwa_large_torpedo;
	aword pwa_phosphorus_torpedo;
	aword pwp_gauss_pistol;
	aword pwp_gauss_rifle;
	aword pwp_heavy_gauss;
	/* pwe = personal weapon: explosive, pgd = personal gadget */
	aword pwe_magna_blast_grenade;
	aword pwe_dye_grenade;
	aword pwe_particle_disturbance_grenade;
	aword pwe_magna_pack_explosive;
	aword pgd_particle_disturbance_sensor;
	aword pgd_medi_kit;
	aword pgd_mc_disruptor;
	aword pwp_thermal_tazer;
	aword pwp_chemical_flare;
	aword pwp_vibro_blade;
	aword pwp_thermic_lance;
	aword pwp_heavy_thermic_lance; /** offset: 0x6a - 106 */
	aword item_unknown01[3];
	aword pwp_sonic_cannon;
	aword pwa_cannon_power_clip;
	aword pwp_sonic_blasta_rifle;
	aword pwa_blasta_power_clip;
	aword pwp_sonic_pistol;
	aword pwa_pistol_power_clip;
	aword pwp_disruptor_pulse_launcher;
	aword pwa_disruptor_ammo;
	aword pwp_thermal_shok_launcher;
	aword pwa_thermal_shok_bomb;
	aword pwe_sonic_pulser;
	aword artifact_zrbyte; /** offset: 0x88 - 136 */
	aword pgd_mc_reader;
	aword pwa_gauss_pistol_clip;
	aword pwa_gauss_rifle_clip;
	aword pwa_heavy_gauss_clip;
	aword corpse_aquatoid;
	aword corpse_gill_man;
	aword corpse_lobster_man;
	aword corpse_tasoth;
	aword corpse_calcinite;
	aword corpse_deep_one;
	aword corpse_biodrone;
	aword corpse_tentaculat;
	aword corpse_triscene;
	aword corpse_hallucinoid;
	aword corpse_xarquid;
	aword item_unknown02[4]; /** live alien pointer? */
	aword artifact_ion_beam_accelarators;
	aword artifact_magnetic_navigation;
	aword artifact_alien_sub_construction;
	aword artifact_alien_cryogenics;
	aword artifact_alien_cloning;
	aword artifact_alien_learning_arrays;
	aword artifact_alien_implanter;
	aword artifact_examination_room;
	aword artifact_aqua_plastics;
	aword artifact_alien_reanimation_zone;
	/* par = personal armor */
	aword par_plastic_aqua_armor;
	aword par_ion_armor;
	aword par_magnetic_ion_armor;
	/* avs = assault vehicle system, ava = assault vehicle ammo */
	aword ava_solid_harpoon_bolts;
	aword ava_aqua_jet_missiles;
	aword ava_pw_torpedo;
	aword ava_gauss_cannon_ammo;
	aword ava_sonic_maybe; /** unused - displacer sonic? */
	aword detection_short; /** 0x00@0x0a@0x14@0x1e */
	aword detection_long; /** 0x00@0x14 */
	aword detection_resolver; /** 0x00@0x64 */
	abyte build_type[BASE_TILE_COUNT]; /** offset: 0x - 218 */
	abyte build_to_finish[BASE_TILE_COUNT];
	abyte count_engineer, count_scientist; /** offset: 0x - 290 */
	adwrd empty_flag; /** 0x0001 if empty? */
} base_t;  /** size: 0x128 */
/*----------------------------------------------------------------------------*/
#define BASE_INFO_SIZE 296
#define BASE_FULLCOUNT 8
#define BASE_FULLBYTES (BASE_INFO_SIZE*BASE_FULLCOUNT)
#define BASE_DATA_NAME "BASE.DAT"
#define BASE_DATA_NAMESZ 8
/*----------------------------------------------------------------------------*/
#define BASE_TILE_AIRLOCK 0x00
#define BASE_TILE_LIVING_QUARTERS 0x01
#define BASE_TILE_LABORATORY 0x02
#define BASE_TILE_WORKSHOP 0x03
#define BASE_TILE_STANDARD_SONAR 0x04
#define BASE_TILE_WIDEARRAY_SONAR 0x05
#define BASE_TILE_GENERAL_STORES 0x07
#define BASE_TILE_ALIEN_CONTAINMENT 0x08
#define BASE_TILE_TORPEDO_DEFENSE 0x06
#define BASE_TILE_GAUSS_DEFENSE 0x09
#define BASE_TILE_SONIC_DEFENSE 0x0A
#define BASE_TILE_PWT_DEFENSE 0x0B
#define BASE_TILE_BOMBARDMENT_SHIELD 0x0C
#define BASE_TILE_MC_DEFENCES 0x0D
#define BASE_TILE_MC_LAB 0x0E
#define BASE_TILE_TRANSMISSION_RESOLVER 0x0F
#define BASE_TILE_SUBPEN_TL 0x10
#define BASE_TILE_SUBPEN_TR 0x11
#define BASE_TILE_SUBPEN_BL 0x12
#define BASE_TILE_SUBPEN_BR 0x13
#define BASE_TILE_TYPECOUNT 0x14
/*----------------------------------------------------------------------------*/
#define BASE_TILE_EMPTY 0xFF
/*----------------------------------------------------------------------------*/
const char XCOM2_BASE_TILE_NAMES[][32] = {
		{ "AirLock" },
		{ "Living Quarters" },
		{ "Laboratory" },
		{ "Workshop" },
		{ "Standard Sonar" },
		{ "Wide-Array Sonar" },
		{ "Torpedo Defense" },
		{ "General Stores" },
		{ "Alien Containment" },
		{ "Gauss Defence" },
		{ "Sonic Defense" },
		{ "PWT Defense" },
		{ "Bombardment Shield" },
		{ "MC Defences" },
		{ "MC Lab" },
		{ "Transmission Resolver" },
		{ "Sub-Pen" },
		{ "" }
};
/*----------------------------------------------------------------------------*/
int base_type_count(base_t* base, int type) {
	int loop, size;
	for (loop=0,size=0;loop<BASE_TILE_COUNT;loop++) {
		if (base->build_type[loop]==type)
			size++;
	}
	return size;
}
/*----------------------------------------------------------------------------*/
#define BASE_INCONSTRUCTION_CHAR '*'
/*----------------------------------------------------------------------------*/
void base_show_layout(base_t* base) {
	int loop, irow, icol, temp;
	int flag, step, tcnt[BASE_TILE_TYPECOUNT];
	for (temp=0;temp<BASE_TILE_TYPECOUNT;temp++)
		tcnt[temp] = base_type_count(base,temp);
	flag=0; loop=0; step = 0;
	for (irow=0;irow<BASE_TILE_ROWS;irow++) {
		printf("   "); 
		for (icol=0;icol<BASE_TILE_COLS;icol++,loop++) {
			temp = base->build_type[loop];
			printf("| "); 
			if (temp!=BASE_TILE_EMPTY) {
				if (temp<BASE_TILE_TYPECOUNT)
					printf("%02X",temp);
				else printf("??");
				if (base->build_to_finish[loop]) {
					putchar(BASE_INCONSTRUCTION_CHAR);
					flag++;
				}
				else putchar(' ');
			}
			else printf("XX ");
		}
		printf("|");
		for (temp=0;temp<2&&step<=BASE_TILE_SUBPEN_TL;step++) {
			if (!tcnt[step]) continue;
			if (!temp) printf("  >");
			else printf(" ,");
			if (step==BASE_TILE_SUBPEN_TL)
				printf(" %02X|%02X|%02X|%02X",step,BASE_TILE_SUBPEN_TR,
					BASE_TILE_SUBPEN_BL,BASE_TILE_SUBPEN_BR);
			else printf(" %02X",step);
			printf(":%s",XCOM2_BASE_TILE_NAMES[step]);
			temp++;
		}
		printf("\n"); 
	}
	if (flag) printf("   %c: In construction.\n",BASE_INCONSTRUCTION_CHAR);
}
/*----------------------------------------------------------------------------*/
void base_show(base_t* base) {
	int type, size, step;
	printf("-- Base Name: %s\n",base->name);
	printf("   > Detection-S:%d  Detection-L:%d  Detection-H:%d\n",
		base->detection_short,base->detection_long,
		base->detection_resolver);
	if (base->count_engineer||base->count_scientist)
		printf("   > Engineers:%d  Scientists:%d\n",
			base->count_engineer,base->count_scientist);
	if (base->cws_sonic_oscillator||base->artifact_zrbyte)
		printf("   > Sonic Oscillator:%d  Zrbyte:%d\n",
			base->cws_sonic_oscillator,base->artifact_zrbyte);
	size = base_type_count(base,BASE_TILE_AIRLOCK);
	if (size!=1)
		fprintf(stderr,"** Invalid base airlock count? (%d)\n",size);
	for (type=1,step=0;type<=0x10;type++) {
		size = base_type_count(base,type);
		if (size>0) {
			if (!step) printf("   #");
			printf(" [%s:%d]",XCOM2_BASE_TILE_NAMES[type],size);
			step++;
			if (step==3) {
				printf("\n");
				step = 0;
			}
		}
	}
	if (step) printf("\n");
}
/*----------------------------------------------------------------------------*/
#define BASE_BUILD0_ENGINEERS 90
#define BASE_BUILD0_SCIENTISTS 100
#define BASE_BUILD0_ZRBYTE 500
#define BASE_BUILD0_SONIC_OSCILLATORS 20
#define BASE_BUILD0_SONIC_BLASTA 20
#define BASE_BUILD0_BLASTA_POWER_CLIP 100
#define BASE_BUILD0_DETECTION_SHORT 100
#define BASE_BUILD0_DETECTION_LONG 100
#define BASE_BUILD0_DETECTION_RESOLVER 100
/*----------------------------------------------------------------------------*/
#define BASE_BUILD0_RIFLE BASE_BUILD0_SONIC_BLASTA
#define BASE_BUILD0_RIFLE_CLIP BASE_BUILD0_BLASTA_POWER_CLIP
#define BASE_BUILD0_HEAVY_THERMIC_LANCE BASE_BUILD0_RIFLE
/*----------------------------------------------------------------------------*/
#define BASE_FRESH1_ZRBYTE (BASE_BUILD0_ZRBYTE/5)
#define BASE_FRESH1_ZRBYTE_MIN (BASE_FRESH1_ZRBYTE/10)
#define BASE_FRESH1_SONIC_BLASTA (BASE_BUILD0_SONIC_BLASTA/4)
#define BASE_FRESH1_BLASTA_POWER_CLIP (BASE_BUILD0_BLASTA_POWER_CLIP/4)
/*----------------------------------------------------------------------------*/
void base_personnel(base_t* base, int ecnt, int scnt) {
	/* provide engineers and scientists */
	base->count_engineer = ecnt;
	base->count_scientist = scnt;
	printf("-- %d engineers and %d scientists assigned to '%s'!\n",
		base->count_engineer,base->count_scientist,base->name);
}
/*----------------------------------------------------------------------------*/
void base_alien_material(base_t* base, int ecnt) {
	/* provide some zrbyte */
	base->artifact_zrbyte = ecnt;
	printf("-- %d zrbyte provided at '%s'!\n",base->artifact_zrbyte,base->name);
}
/*----------------------------------------------------------------------------*/
int base_alien_material_fresh(base_t* base, int ecnt, int emin) {
	int stat = 0;
	if (base->artifact_zrbyte<emin) {
		base_alien_material(base,ecnt);
		stat++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
void base_alien_research(base_t* base) {
	/* provide alien samples */
	base->corpse_aquatoid = 1;
	base->corpse_gill_man = 1;
	base->corpse_lobster_man = 1;
	base->corpse_tasoth = 1;
	base->corpse_calcinite = 1;
	base->corpse_deep_one = 1;
	base->corpse_biodrone = 1;
	base->corpse_tentaculat = 1;
	base->corpse_triscene = 1;
	base->corpse_hallucinoid = 1;
	base->corpse_xarquid = 1;
	printf("-- All known alien samples provided at '%s'!\n",base->name);
}
/*----------------------------------------------------------------------------*/
void base_craft_weapon(base_t* base, int scnt) {
	/* provide craft weapon - shoulw we consider pwt launcher? */
	base->cws_sonic_oscillator = scnt;
	printf("-- %d sonic oscillators provided at '%s'!\n",
		base->cws_sonic_oscillator,base->name);
}
/*----------------------------------------------------------------------------*/
int base_check_pweapon_melee(base_t* base, int wcnt, int wmin) {
	int stat = 0;
	if (base->pwp_heavy_thermic_lance<wmin) {
		base->pwp_heavy_thermic_lance = wcnt;
		printf("-- %d heavy thermic lance provided at '%s'!\n",
			base->pwp_heavy_thermic_lance,base->name);
		stat++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int base_personnel_weapon_fresh(base_t* base, int wcnt, int wmin) {
	int stat = 0;
	/* provide personnel weapon - sould we consider heavy sonic? */
	if (base->pwp_sonic_blasta_rifle<wmin) {
		base->pwp_sonic_blasta_rifle = wcnt;
		printf("-- %d sonic blasta rifle provided at '%s'!\n",
			base->pwp_sonic_blasta_rifle,base->name);
		stat++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
int base_personnel_weapon_ammo_fresh(base_t* base, int acnt, int amin) {
	int stat = 0;
	if (base->pwa_blasta_power_clip<amin) {
		base->pwa_blasta_power_clip = acnt;
		printf("-- %d blasta power clip provided at '%s'!\n",
			base->pwa_blasta_power_clip,base->name);
		stat++;
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
void base_personnel_weapon(base_t* base, int wcnt, int acnt) {
	base_personnel_weapon_fresh(base,wcnt,wcnt);
	base_personnel_weapon_ammo_fresh(base,acnt,acnt);
	base_check_pweapon_melee(base,wcnt,wcnt);
}
/*----------------------------------------------------------------------------*/
int base_detection_build_type(base_t* base, int type) {
	int stat = 0;
	if (type==BASE_TILE_STANDARD_SONAR) {
		/* max short-range is actually 30 */
		if (base->detection_short<BASE_BUILD0_DETECTION_SHORT) {
			printf("-- Short range detection:%d >> ",base->detection_short);
			base->detection_short = BASE_BUILD0_DETECTION_SHORT;
			printf("%d at '%s'\n",base->detection_short,base->name);
			stat++;
		}
	}
	else if (type==BASE_TILE_WIDEARRAY_SONAR) {
		/* max long-range is actually 20 */
		if (base->detection_long<BASE_BUILD0_DETECTION_LONG) {
			printf("-- Long range detection:%d >> ",base->detection_long);
			base->detection_long = BASE_BUILD0_DETECTION_LONG;
			printf("%d at '%s'\n",base->detection_long,base->name);
			stat++;
		}
	}
	else if (type==BASE_TILE_TRANSMISSION_RESOLVER) {
		if (base->detection_resolver<BASE_BUILD0_DETECTION_RESOLVER) {
			printf("-- Resolver detection:%d >> ",base->detection_resolver);
			base->detection_resolver = BASE_BUILD0_DETECTION_RESOLVER;
			printf("%d at '%s'\n",base->detection_resolver,base->name);
			stat++;
		}
	}
	return stat;
}
/*----------------------------------------------------------------------------*/
void base_build0(base_t* base) {
	int loop;
	/* perfect build */
	const abyte build0[BASE_TILE_COUNT] = {
		BASE_TILE_PWT_DEFENSE,BASE_TILE_PWT_DEFENSE,
		BASE_TILE_SUBPEN_TL,BASE_TILE_SUBPEN_TR,
		BASE_TILE_WORKSHOP,BASE_TILE_ALIEN_CONTAINMENT,
		BASE_TILE_PWT_DEFENSE,BASE_TILE_PWT_DEFENSE,
		BASE_TILE_SUBPEN_BL,BASE_TILE_SUBPEN_BR,
		BASE_TILE_WORKSHOP,BASE_TILE_LABORATORY,
		BASE_TILE_TRANSMISSION_RESOLVER,BASE_TILE_BOMBARDMENT_SHIELD,
		BASE_TILE_AIRLOCK,BASE_TILE_LIVING_QUARTERS,
		BASE_TILE_WORKSHOP,BASE_TILE_LABORATORY,
		BASE_TILE_STANDARD_SONAR,BASE_TILE_WIDEARRAY_SONAR,
		BASE_TILE_GENERAL_STORES,BASE_TILE_LIVING_QUARTERS,
		BASE_TILE_LIVING_QUARTERS,BASE_TILE_LIVING_QUARTERS,
		BASE_TILE_SUBPEN_TL,BASE_TILE_SUBPEN_TR,
		BASE_TILE_GENERAL_STORES,BASE_TILE_GENERAL_STORES,
		BASE_TILE_SUBPEN_TL,BASE_TILE_SUBPEN_TR,
		BASE_TILE_SUBPEN_BL,BASE_TILE_SUBPEN_BR,
		BASE_TILE_GENERAL_STORES,BASE_TILE_GENERAL_STORES,
		BASE_TILE_SUBPEN_BL,BASE_TILE_SUBPEN_BR
	};
	/* MUST BE existing! */
	if (!base->name[0]||base->empty_flag) return;
	/* create full base */
	for (loop=0;loop<BASE_TILE_COUNT;loop++) {
		base->build_type[loop] = build0[loop];
		base->build_to_finish[loop] = 0x0;
		/* adjust detection meter */
		base_detection_build_type(base,build0[loop]);
	}
	printf("-- Complete facilities built in '%s'!\n",base->name);
	/* provide engineers and scientists */
	base_personnel(base,BASE_BUILD0_ENGINEERS,BASE_BUILD0_SCIENTISTS);
	/* some valuable material */
	base_alien_material(base,BASE_BUILD0_ZRBYTE);
	/* some things to research */
	base_alien_research(base);
	/* provide craft weapon - usable */
	base_craft_weapon(base,BASE_BUILD0_SONIC_OSCILLATORS);
	/* provide soldier weapon/ammo - need research! */
	base_personnel_weapon(base,BASE_BUILD0_RIFLE,BASE_BUILD0_RIFLE_CLIP);
}
/*----------------------------------------------------------------------------*/
int base_freshX(base_t* base, unsigned int pick) {
	int stat = 0, loop, temp, done;
	if (!base->name[0]||base->empty_flag) /* must exists! */
		return stat;
	/* finish all builds */
	for (loop=0;loop<BASE_TILE_COUNT;loop++) {
		temp = base->build_type[loop];
		if (temp<0x14) {
			done = base->build_to_finish[loop];
			if (done>0&&done<BASE_TILE_EMPTY) {
				if (temp<=0x10) {
					printf("-- Completing building '%s' for '%s'!\n",
						XCOM2_BASE_TILE_NAMES[temp],base->name);
				}
				base->build_to_finish[loop]= 0x0;
				stat++;
			}
		}
		stat += base_detection_build_type(base,temp);
	}
	stat += base_alien_material_fresh(base,
		BASE_FRESH1_ZRBYTE,BASE_FRESH1_ZRBYTE_MIN);
	return stat;
}
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
int base_load(base_t* base, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += BASE_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,BASE_DATA_NAME);
	pdat = fopen(buff,"rb");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk!=BASE_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			test = BASE_FULLCOUNT-offs;
			if (size>test) size = test;
			test = offs*BASE_INFO_SIZE;
			fseek(pdat,test,SEEK_SET);
			temp = fread((void*)base,BASE_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot read %s!\n",buff);
				stat = -3;
			}
			else {
				stat = size;
				g_xcom2_path = path;
			}
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
int base_save(base_t* base, char* path, int size, int offs) {
	FILE *pdat;
	char *buff;
	long pchk, temp;
	int stat, test;
	test = 0; while (path[test]) test++;
	test += BASE_DATA_NAMESZ+2; /* sep & nul */
	buff = (char*)malloc(test);
	if (!buff) {
		fprintf(stderr,"** Failed malloc() for filename buffer!\n");
		return -1;
	}
	snprintf(buff,test,"%s/%s",path,BASE_DATA_NAME);
	pdat = fopen(buff,"rb+");
	if (pdat) {
		fseek(pdat,0,SEEK_END);
		pchk = ftell(pdat);
		if (pchk!=BASE_FULLBYTES) {
			fprintf(stderr,"** Invalid size for %s!\n",buff);
			stat = -2;
		}
		else {
			test = BASE_FULLCOUNT-offs;
			if (size>test) size = test;
			test = offs*BASE_INFO_SIZE;
			fseek(pdat,test,SEEK_SET);
			temp = fwrite((void*)base,BASE_INFO_SIZE,size,pdat);
			if (temp!=size) {
				fprintf(stderr,"** Cannot read %s!\n",buff);
				stat = -3;
			}
			else stat = size;
		}
		fclose(pdat);
	}
	else {
		fprintf(stderr,"** Cannot open %s!\n",buff);
		stat = -1;
	}
	free((void*)buff);
	return stat;
}
/*----------------------------------------------------------------------------*/
int base_name_index(char* buff, int size, int indx) {
	base_t base;
	int test;
	test = base_load(&base,g_xcom2_path,1,indx);
	if (test==1) {
		test = snprintf(buff,size,"%s",base.name);
		if (test<1||test>=size)
			test = 0;
		buff[test] = 0x0;
	}
	else test = 0;
	return test;
}
/*----------------------------------------------------------------------------*/
#endif /* __MY1XCOM2_BASE_H__ */
/*----------------------------------------------------------------------------*/
