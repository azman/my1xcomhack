/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define DEFAULT_SAVEPATH "."
#define MAXCHAR_PATHFILE 512
/*----------------------------------------------------------------------------*/
#define FLAG_DOSAVE 0x80
#define FLAG_BUILD0 0x04
#define FLAG_FRESH1 0x01
#define FLAG_CHKPWP 0x20
#define FLAG_CHKPWA 0x10
#define FLAG_HACK00 (FLAG_CHKPWP|FLAG_CHKPWA)
#define FLAG_HACKED (FLAG_BUILD0|FLAG_FRESH1|FLAG_HACK00)
/*----------------------------------------------------------------------------*/
#include "xcom2_base.h"
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	base_t base[BASE_FULLCOUNT];
	int loop, test, pick, offs, size, flag;
	int wcnt, acnt;
	char *path, *ptmp, *pchk;
	char full[MAXCHAR_PATHFILE];
	char pdef[] = DEFAULT_SAVEPATH;
	path = pdef;
	/* validate! */
	if (sizeof(base_t)!=BASE_INFO_SIZE) {
		fprintf(stderr,"** Invalid Base data structure! (%d|%d)\n",
			(int)sizeof(base_t),(int)BASE_INFO_SIZE);
		return -1;
	}
	/* default setting */
	pick = 1; offs = 0; size = BASE_FULLCOUNT; flag = 0;
	/* check program args */
	if (argc>0) {
		for (loop=1; loop<argc; loop++) {
			pchk = argv[loop];
			if (pchk[0]=='-') {
				if (pchk[1]=='-') {
					ptmp = &pchk[2];
					if (strcmp(ptmp,"offs")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<0||test>=BASE_FULLCOUNT)
							fprintf(stderr,"** Invalid offset!\n");
						else offs = test;
					}
					else if (strcmp(ptmp,"size")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>(BASE_FULLCOUNT-offs))
							fprintf(stderr,"** Invalid size!\n");
						else size = test;
					}
					else if (strcmp(ptmp,"rifle")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>BASE_BUILD0_RIFLE)
							fprintf(stderr,"** Invalid size!\n");
						else {
							wcnt = test;
							flag |= FLAG_CHKPWP;
						}
					}
					else if (strcmp(ptmp,"rifle-clip")==0) {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>BASE_BUILD0_RIFLE_CLIP)
							fprintf(stderr,"** Invalid size!\n");
						else {
							acnt = test;
							flag |= FLAG_CHKPWA;
						}
					}
					else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
				}
				else if (!pchk[2]) {
					if (pchk[1]=='g') {
						if (++loop==argc) break;
						pick = atoi(argv[loop]);
						if (pick<0||pick>10) {
							fprintf(stderr,"** Invalid game index (1-10)!\n");
							pick = 1;
						}
					}
					else if (pchk[1]=='p') {
						if (++loop==argc) break;
						path = argv[loop];
					}
					else if (pchk[1]=='i') {
						if (++loop==argc) break;
						test = atoi(argv[loop]);
						if (test<1||test>BASE_FULLCOUNT) {
							fprintf(stderr,"** Invalid base index (1-%d)!\n",
								BASE_FULLCOUNT);
						}
						else {
							offs = test-1;
							size = 1;
						}
					}
					else if (pchk[1]=='f') flag |= FLAG_FRESH1;
					else if (pchk[1]=='u') flag |= FLAG_BUILD0;
					else if (pchk[1]=='w') flag |= FLAG_DOSAVE;
					else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
				}
				else fprintf(stderr,"@@ Unknown option '%s'!\n",pchk);
			}
			else fprintf(stderr,"@@ Unknown option '%s'!\n",argv[loop]);
		}
	}
	/* copy path name */
	sprintf(full,"%s/GAME_%d",path,pick);
	printf("@@ Processing %s\n",full);
	/* go to work */
	test = base_load(base,full,size,offs);
	if (test>0) {
		printf("## Base data loaded! (%d)\n",test);
		test = 0;
		for (loop=0;loop<size;loop++) {
			if (base[loop].empty_flag||!base[loop].name[0])
				continue;
			if (!(flag&FLAG_HACKED)) {
				base_show(&base[loop]);
				if (size==1) base_show_layout(&base[loop]);
			}
			if (flag&FLAG_FRESH1)
				pick = base_freshX(&base[loop],0);
			else if (flag&FLAG_BUILD0) {
				base_build0(&base[loop]);
				pick = 1;
			}
			else pick = 0;
			if (flag&FLAG_CHKPWP)
				pick += base_personnel_weapon_fresh(&base[loop],wcnt,wcnt);
			if (flag&FLAG_CHKPWA)
				pick += base_personnel_weapon_ammo_fresh(&base[loop],acnt,acnt);
			if (pick) {
				base_show(&base[loop]);
				test += pick;
			}
		}
		if (test&&(flag&FLAG_DOSAVE)) {
			test = base_save(base,full,size,offs);
			if (test>0) printf("## Changes saved!\n");
			else fprintf(stderr,"** Error saving changes to '%s'!\n",full);
		}
	}
	else fprintf(stderr,"** Error loading data from '%s'!\n",full);
	/* done */
	return 0;
}
/*----------------------------------------------------------------------------*/
