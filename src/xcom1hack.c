/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define DEFAULT_SAVEPATH "."
/*----------------------------------------------------------------------------*/
#include "xcom1_data.h"
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	xcom1_data_t xcom;
	int loop, pick = 1;
	char fpath[] = DEFAULT_SAVEPATH, *ppath = fpath;
	char fullpath[MAXCHAR_PATHFILE];
	/* validate! */
	if (sizeof(base_t)!=BASE_INFO_SIZE) {
		fprintf(stderr,"** Invalid base data structure! (%d|%d)\n",
			(int)sizeof(base_t),(int)BASE_INFO_SIZE);
		return -1;
	}
	if (sizeof(soldier_t)!=SOLDIER_INFO_SIZE) {
		fprintf(stderr,"** Invalid soldier data structure! (%d|%d)\n",
			(int)sizeof(soldier_t),(int)SOLDIER_INFO_SIZE);
		return -1;
	}
	xcom1_init(&xcom);
	/* check program args */
	if (argc>0) {
		for (loop=1; loop<argc; loop++) {
			if (strcmp(argv[loop],"-g")==0) {
				if(++loop==argc) break;
				pick = atoi(argv[loop]);
				if (pick<0||pick>10) {
					fprintf(stderr,"** Invalid game index (1-10)!\n");
					pick = 1;
				}
			}
			else if(strcmp(argv[loop],"-p")==0) {
				if(++loop==argc) break;
				ppath = argv[loop];
			}
			else if(strcmp(argv[loop],"-l")==0)
				xcom.flag|=XCOM1_FLAG_SHOW_ALL;
			else if(strcmp(argv[loop],"-lb")==0)
				xcom.flag|=XCOM1_FLAG_SHOW_BASE;
			else if(strcmp(argv[loop],"-ls")==0)
				xcom.flag|=XCOM1_FLAG_SHOW_SOLDIER;
			else if(strcmp(argv[loop],"-i")==0)
				xcom.flag|=XCOM1_FLAG_INITIALIZE;
			else if(strcmp(argv[loop],"-r")==0)
				xcom.flag|=XCOM1_FLAG_REFRESH;
			else if(strcmp(argv[loop],"-rb")==0)
				xcom.flag|=XCOM1_FLAG_FRESH_BASE;
			else if(strcmp(argv[loop],"-rs")==0)
				xcom.flag|=XCOM1_FLAG_FRESH_SOLDIER;
			else if(strcmp(argv[loop],"-rf")==0)
				xcom.flag|=XCOM1_FLAG_FRESH_FUND;
			else if(strcmp(argv[loop],"-w")==0)
				xcom.flag|=XCOM1_FLAG_COMMIT;
			else if(strcmp(argv[loop],"--supers")==0)
				xcom.flag|=XCOM1_FLAG_SUPER_SOLDIERS;
			else
				fprintf(stderr,"@@ Unknown option '%s'!\n",argv[loop]);
		}
	}
	/* copy path name */
	sprintf(fullpath,"%s/GAME_%d",ppath,pick);
	printf("@@ Processing %s\n",fullpath);
	/* default work */
	if (!xcom.flag) xcom.flag|=XCOM1_FLAG_SHOW_BASE;
	/* go to work */
	xcom1_data_load(&xcom,fullpath);
	xcom1_data_show(&xcom);
	xcom1_data_hack(&xcom);
	xcom1_data_save(&xcom);
	/* done */
	return xcom.stat?-1:0;
}
/*----------------------------------------------------------------------------*/
