#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT_SAVEPATH "."
#define MAXCHAR_PATHFILE 512

#define ERROR_NONE 0
#define ERROR_COMMAND -1
#define ERROR_FILE -2

typedef unsigned char  abyte;
typedef unsigned short aword;
typedef unsigned int   adwrd;

#define BASE_NAME_SIZE 14
#define BASE_TILE_COUNT 36
#define BASE_TILE_AIRLOCK 0x00
#define BASE_TILE_LIVING_QUARTERS 0x01
#define BASE_TILE_LABORATORY 0x02
#define BASE_TILE_WORKSHOP 0x03
#define BASE_TILE_STANDARD_SONAR 0x04
#define BASE_TILE_WIDEARRAY_SONAR 0x05
#define BASE_TILE_GENERAL_STORES 0x07
#define BASE_TILE_ALIEN_CONTAINMENT 0x08
#define BASE_TILE_TORPEDO_DEFENSE 0x06
#define BASE_TILE_GAUSS_DEFENSE 0x09
#define BASE_TILE_SONIC_DEFENSE 0x0A
#define BASE_TILE_PWT_DEFENSE 0x0B
#define BASE_TILE_BOMBARDMENT_SHIELD 0x0C
#define BASE_TILE_MC_DEFENCES 0x0D
#define BASE_TILE_MC_LAB 0x0E
#define BASE_TILE_TRANSMISSION_RESOLVER 0x0F
#define BASE_TILE_SUBPEN_TL 0x10
#define BASE_TILE_SUBPEN_TR 0x11
#define BASE_TILE_SUBPEN_BL 0x12
#define BASE_TILE_SUBPEN_BR 0x13
#define BASE_TILE_EMPTY 0xFF
#define BASE_BUILT_TAG 0x0000

typedef struct __base {
	abyte name[BASE_NAME_SIZE];
	aword name_sep; /** null */
	/* cws = craft weapon system, cwa = craft weapon ammo */
	aword cws_ajax_launcher;
	aword cws_dup_head_launcher;
	aword cws_craft_gas_cannon;
	aword cws_pwt_cannon;
	aword cws_gauss_cannon;
	aword cws_sonic_oscillator;
	aword cwa_ajax_torpedos;
	aword cwa_dup_head_torpedos;
	aword cwa_gas_rounds_x50;
	aword cwa_pwt_ammo;
	/* avs = assault vehicle system, ava = assault vehicle ammo */
	aword avs_coelacanth_gcannon;
	aword avs_coelacanth_aquajet;
	aword avs_coelacanth_gauss;
	aword avs_displacer_sonic;
	aword avs_displacer_pwt;
	/* pwp = personal weapon, pwa = personal weapon ammo */
	aword pwp_dart_gun;
	aword pwa_dart_clip;
	aword pwp_jet_harpoon;
	aword pwa_harpoon_clip;
	aword pwp_gas_cannon;
	aword pwa_gc_ap_bolts;
	aword pwa_gc_he_bolts;
	aword pwa_gc_phosphorus_bolts;
	aword pwp_hydro_jet_cannon;
	aword pwa_hj_ap_ammo;
	aword pwa_hj_he_ammo;
	aword pwa_hj_phosphorus_ammo;
	aword pwp_torpedo_launcher;
	aword pwa_small_torpedo;
	aword pwa_large_torpedo;
	aword pwa_phosphorus_torpedo;
	aword pwp_gauss_pistol;
	aword pwp_gauss_rifle;
	aword pwp_heavy_gauss;
	/* pwe = personal weapon: explosive, pgd = personal gadget */
	aword pwe_magna_blast_grenade;
	aword pwe_dye_grenade;
	aword pwe_particle_disturbance_grenade;
	aword pwe_magna_pack_explosive;
	aword pgd_particle_disturbance_sensor;
	aword pgd_medi_kit;
	aword pgd_mc_disruptor;
	aword pwp_thermal_tazer;
	aword pwp_chemical_flare;
	aword pwp_vibro_blade;
	aword pwp_thermic_lance;
	aword pwp_heavy_thermic_lance; /** offset: 0x6a - 106 */
	aword item_unknown01[3];
	aword pwp_sonic_cannon;
	aword pwa_cannon_power_clip;
	aword pwp_sonic_blasta_rifle;
	aword pwa_blasta_power_clip;
	aword pwp_sonic_pistol;
	aword pwa_pistol_power_clip;
	aword pwp_disruptor_pulse_launcher;
	aword pwa_disruptor_ammo;
	aword pwp_thermal_shok_launcher;
	aword pwa_thermal_shok_bomb;
	aword pwe_sonic_pulser;
	aword artifact_zrbyte; /** offset: 0x88 - 136 */
	aword pgd_mc_reader;
	aword pwa_gauss_pistol_clip;
	aword pwa_gauss_rifle_clip;
	aword pwa_heavy_gauss_clip;
	aword corpse_aquatoid;
	aword corpse_gill_man;
	aword corpse_lobster_man;
	aword corpse_tasoth;
	aword corpse_calcinite;
	aword corpse_deep_one;
	aword corpse_biodrone;
	aword corpse_tentaculat;
	aword corpse_triscene;
	aword corpse_hallucinoid;
	aword corpse_xarquid;
	aword item_unknown02[4]; /** live alien pointer? */
	aword artifact_ion_beam_accelarators;
	aword artifact_magnetic_navigation;
	aword artifact_alien_sub_construction;
	aword artifact_alien_cryogenics;
	aword artifact_alien_cloning;
	aword artifact_alien_learning_arrays;
	aword artifact_alien_implanter;
	aword artifact_examination_room;
	aword artifact_aqua_plastics;
	aword artifact_alien_reanimation_zone;
	/* par = personal armor */
	aword par_plastic_aqua_armor;
	aword par_ion_armor;
	aword par_magnetic_ion_armor;
	/* avs = assault vehicle system, ava = assault vehicle ammo */
	aword ava_solid_harpoon_bolts;
	aword ava_aqua_jet_missiles;
	aword ava_pw_torpedo;
	aword ava_gauss_cannon_ammo;
	aword ava_sonic_maybe; /** unused - displacer sonic? */
	aword detection_short; /** 0x00@0x0a@0x14@0x1e */
	aword detection_long; /** 0x00@0x14 */
	aword detection_resolver; /** 0x00@0x64 */
	abyte build_type[BASE_TILE_COUNT]; /** offset: 0x - 218 */
	abyte build_to_finish[BASE_TILE_COUNT];
	abyte count_engineer, count_scientist; /** offset: 0x - 290 */
	adwrd empty_tag; /** 0x0001 if empty? */
} base_t; /** size: 0x128 */
#define BASE_INFO_SIZE 296

int base_upgrade(char* fullpath, int do_init) {
	int loop;
	base_t tbase;
	char fullname[MAXCHAR_PATHFILE];
	abyte dbasetiles[BASE_TILE_COUNT] = {
		BASE_TILE_PWT_DEFENSE,BASE_TILE_PWT_DEFENSE,
		BASE_TILE_SUBPEN_TL,BASE_TILE_SUBPEN_TR,
		BASE_TILE_WORKSHOP,BASE_TILE_ALIEN_CONTAINMENT,
		BASE_TILE_PWT_DEFENSE,BASE_TILE_PWT_DEFENSE,
		BASE_TILE_SUBPEN_BL,BASE_TILE_SUBPEN_BR,
		BASE_TILE_WORKSHOP,BASE_TILE_LABORATORY,
		BASE_TILE_TRANSMISSION_RESOLVER,BASE_TILE_BOMBARDMENT_SHIELD,
		BASE_TILE_AIRLOCK,BASE_TILE_LIVING_QUARTERS,
		BASE_TILE_WORKSHOP,BASE_TILE_LABORATORY,
		BASE_TILE_STANDARD_SONAR,BASE_TILE_WIDEARRAY_SONAR,
		BASE_TILE_GENERAL_STORES,BASE_TILE_LIVING_QUARTERS,
		BASE_TILE_LIVING_QUARTERS,BASE_TILE_LIVING_QUARTERS,
		BASE_TILE_SUBPEN_TL,BASE_TILE_SUBPEN_TR,
		BASE_TILE_GENERAL_STORES,BASE_TILE_GENERAL_STORES,
		BASE_TILE_SUBPEN_TL,BASE_TILE_SUBPEN_TR,
		BASE_TILE_SUBPEN_BL,BASE_TILE_SUBPEN_BR,
		BASE_TILE_GENERAL_STORES,BASE_TILE_GENERAL_STORES,
		BASE_TILE_SUBPEN_BL,BASE_TILE_SUBPEN_BR
	};
	FILE *basefile;
	/* set base full file name */
	strcpy(fullname,fullpath);
	strcat(fullname,"BASE.DAT");
	/* open file */
	basefile = fopen(fullname,"rb+");
	if (!basefile) {
		printf("File %s not found!\n",fullname);
		return ERROR_FILE;
	}
	while (1) {
		fread((void*)&tbase,sizeof(base_t),1,basefile);
		if(tbase.empty_tag||tbase.name[0]==0x0) break;
		printf("Base found: '%s'\n",(char*)tbase.name);
	}
	fseek(basefile,0x0,SEEK_SET);
	/* read base info */
	fread((void*)&tbase,sizeof(base_t),1,basefile);
	/* check if requesting full base */
	if (do_init) {
		/* create full base */
		for (loop=0;loop<BASE_TILE_COUNT;loop++) {
			tbase.build_type[loop] = dbasetiles[loop];
			tbase.build_to_finish[loop] = 0x0;
		}
		/* adjust detection meter */
		tbase.detection_resolver = 0x64;
		tbase.detection_long = 0x14;
		tbase.detection_short = 0x1e;
		printf("Nice facilities built in first base!\n");
		/* provide 90 engineers and 100 scientists */
		tbase.count_engineer = 0x5A;
		tbase.count_scientist = 0x64;
		printf("90 engineers and 100 scientists provided at first base!\n");
		/* provide craft weapon */
		tbase.cws_sonic_oscillator = 50;
		printf("%d sonic oscillators added to first base!\n",
			tbase.cws_sonic_oscillator);
		/* provide small alien weapon */
		tbase.pwp_sonic_pistol = 10;
		tbase.pwa_pistol_power_clip = 50;
		printf("%d sonic pistol & %d clips added to first base!\n",
			tbase.pwp_sonic_pistol,tbase.pwa_pistol_power_clip);
		/* provide substantial amount of zrbyte */
		tbase.artifact_zrbyte = 1000;
		printf("%d zrbyte added to first base!\n",tbase.artifact_zrbyte);
		/* provide alien samples */
		tbase.corpse_aquatoid = 1;
		tbase.corpse_gill_man = 1;
		tbase.corpse_lobster_man = 1;
		tbase.corpse_tasoth = 1;
		tbase.corpse_calcinite = 1;
		tbase.corpse_deep_one = 1;
		tbase.corpse_biodrone = 1;
		tbase.corpse_tentaculat = 1;
		tbase.corpse_triscene = 1;
		tbase.corpse_hallucinoid = 1;
		tbase.corpse_xarquid = 1;
		printf("All known alien samples provided at first base!\n");
		/* just testing */
		tbase.artifact_alien_sub_construction = 1;
	}
	/* back to beginning of file */
	fseek(basefile,0x0,SEEK_SET);
	fwrite((void*)&tbase,sizeof(base_t),1,basefile);
	/* close file */
	fclose(basefile);
	printf("File %s patched!\n",fullname);
	return ERROR_NONE;
}

int bank_upgrade(char* fullpath, int do_init) {
	char fullname[MAXCHAR_PATHFILE];
	FILE *bankfile;
	adwrd money = 0x03ffffff;
	/* set bank full file name */
	strcpy(fullname,fullpath);
	strcat(fullname,"LIGLOB.DAT");
	/* open file */
	bankfile = fopen(fullname,"rb+");
	if (!bankfile) {
		printf("File %s not found!\n",fullname);
		return ERROR_FILE;
	}
	if (do_init) {
		fwrite((void*)&money,sizeof(adwrd),1,bankfile);
		printf("Amount %d added to your bank!\n",money);
	}
	/* close file */
	fclose(bankfile);
	printf("File %s patched!\n",fullname);
	return ERROR_NONE;
}

#define SOLDIER_CRAFT_NONE 0xFFFF
#define SOLDIER_RANK_INVALID 0xFFFF
#define SOLDIER_RANK_DEAD 0xFFFF
#define SOLDIER_RANK_SEAMAN 0x0000
#define SOLDIER_RANK_COMMANDER 0x0004
#define SOLDIER_RANK_CAPTAIN 0x0005
#define SOLDIER_ARMOR_MAGNETIC_ION 0x03

typedef struct __soldier {
	abyte mc_skill_plus, tu_plus, mc_implant_flag, promotion_flag;
	abyte health_plus, strength_plus;
	aword base; /** 0x0000 is first base, refer to LOC.DAT */
	aword craft;
	aword rank;
	aword prev_craft; /** always 0xFFFF?  actually craft before wounded */
	aword kills;
	aword recovery;
	aword missions;
	aword value; /** huh? */
	abyte transfer_flag; /** if non-zero destination base */
	abyte stamina_plus, reaction_plus,firing_accuracy_plus;
	abyte time_unit, health, stamina, throwing_accuracy;
	abyte armor, gender, race, firing_accuracy, strength;
	abyte name[20];
	abyte name_plus[7]; /** no null needed? */
	abyte bravery_plus, throwing_accuracy_plus;
	abyte bravery;
	abyte mc_skill, melee_accuracy_plus;
	abyte mc_strength, melee_accuracy, reaction;
} soldier_t;
#define SOLDIER_INFO_SIZE 70

#define SOLDIER_MAX_COUNT 10
#define SOLDIER_BRAVERY(x) ((110-x)/10)

int army_upgrade(char* fullpath, int do_init) {
	int loop, more = 1;
	soldier_t rambo[SOLDIER_MAX_COUNT];
	char fullname[MAXCHAR_PATHFILE];
	FILE *armyfile;
	/* set soldier full file name */
	strcpy(fullname,fullpath);
	strcat(fullname,"SOLDIER.DAT");
	/* open file */
	armyfile = fopen(fullname,"rb+");
	if (!armyfile) {
		printf("File %s not found!\n",fullname);
		return ERROR_FILE;
	}

	/* get 10 ordinary soldiers and make 10 super soldiers! */
	for (loop=0;loop<10;loop++) {
		fread((void*)&rambo[loop],sizeof(soldier_t),1,armyfile);
		if (do_init||rambo[loop].rank==SOLDIER_RANK_DEAD) {
			rambo[loop].craft = SOLDIER_CRAFT_NONE;
			rambo[loop].rank = SOLDIER_RANK_COMMANDER;
			rambo[loop].prev_craft = SOLDIER_CRAFT_NONE;
			if (rambo[loop].name[0]==0x0)
				sprintf((char*)rambo[loop].name,"Maverick %d",more++);
			rambo[loop].time_unit = 0x96;
			rambo[loop].health = 0x96;
			rambo[loop].stamina = 0x96;
			rambo[loop].throwing_accuracy = 0x96;
			rambo[loop].armor = SOLDIER_ARMOR_MAGNETIC_ION;
			rambo[loop].firing_accuracy = 0x96;
			rambo[loop].melee_accuracy = 0x96;
			rambo[loop].strength = 0x96;
			rambo[loop].bravery = SOLDIER_BRAVERY(100);
			rambo[loop].reaction = 0x96;
			rambo[loop].mc_strength = 0x96;
			rambo[loop].mc_skill = 0x96;
			printf("Making %s a super soldier!\n",(char*)rambo[loop].name);
		}
		if (rambo[loop].recovery) {
			rambo[loop].recovery = 0;
			printf("Full recovery for wounded %s!\n",(char*)rambo[loop].name);
		}
		if (rambo[loop].bravery!=SOLDIER_BRAVERY(100)) {
			rambo[loop].bravery = SOLDIER_BRAVERY(100);
			printf("Restoring bravery for %s!\n",(char*)rambo[loop].name);
		}
	}
	/* back to beginning of file */
	fseek(armyfile,0x0,SEEK_SET);
	for (loop=0;loop<10;loop++) {
		fwrite((void*)&rambo[loop],sizeof(soldier_t),1,armyfile);
	}
	/* close file */
	fclose(armyfile);
	printf("File %s patched!\n",fullname);
	return ERROR_NONE;
}

int main(int argc, char *argv[]) {
	int loop, game_index = 1, game_init = 0, game_cheat = 0;
	char fpath[] = DEFAULT_SAVEPATH, *ppath = fpath;
	char fullpath[MAXCHAR_PATHFILE];

	if (argc>0) {
		for (loop=1; loop<argc; loop++) {
			if (strcmp(argv[loop],"-g")==0) {
				if (++loop==argc) break;
				game_index = atoi(argv[loop]);
				if (game_index<0||game_index>10) {
					fprintf(stderr,"Invalid game index (1-10)!\n");
					return ERROR_FILE;
				}
			}
			else if (strcmp(argv[loop],"-p")==0) {
				if (++loop==argc) break;
				ppath = argv[loop];
			}
			else if (strcmp(argv[loop],"-i")==0) game_init = 1;
			else if(strcmp(argv[loop],"-w")==0) game_cheat = 1;
			else {
				fprintf(stderr,"Unknown option '%s'!\n",argv[loop]);
				return ERROR_COMMAND;
			}
		}
	}

	/* copy path name */
	sprintf(fullpath,"%s/GAME_%d/",ppath,game_index);

	printf("Base datasize = %d (%d)\n",(int)sizeof(base_t),BASE_INFO_SIZE);
	printf("Soldier datasize = %d (%d)\n",(int)sizeof(soldier_t),
		SOLDIER_INFO_SIZE);

	/* modify only if requested */
	if (game_cheat) {
		/* upgrade base */
		base_upgrade(fullpath,game_init);
		/* upgrade money */
		bank_upgrade(fullpath,game_init);
		/* upgrade soldiers */
		army_upgrade(fullpath,game_init);
	}
	return ERROR_NONE;
}
