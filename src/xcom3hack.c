#include <stdio.h>
#include <string.h>

//#define __KDEBUG__ 1

#define OFFSET_LABEL 0x28 /* 40 */
#define LABEL_MAXCHAR 40
#define MAXCOUNT_ORGS 28

#define OFFSET_BUILDING 0x17e4e /* 97870 */
#define MAXCOUNT_BUILDING 109
#define BUILDOWN_XCOM 0x0100 /* 256 */
#define BUILDING_SIZE 226

#define OFFSET_BASE 0x1df6a /* 122730 */
#define MAXCOUNT_BASE 8
#define BASE_MAXCHAR 20
#define BASE_MAXCHAR_LIM 18
#define BASE_TILE_COUNT 64
#define BASE_SIZE 702

#define OFFSET_AGENT 0x20d6a /* 134506 */
#define MAXCOUNT_AGENT 180
#define AGENT_MAXCHAR 26
#define AGENT_MAXCHAR_LIM 24
#define AGENT_SIZE 206

#define OFFSET_FUNDING 0x33d40 /* 212288 */
#define MAXCOUNT_FUNDING MAXCOUNT_ORGS
#define FUNDING_SIZE 438

#define MAX_BUILD_TILE 34
#define FUNDING_MAX 2000000000
#define AGENT_POWER_SUPER 100
#define AGENT_PWRPP_SUPER 100
#define AGENT_SKILL_SUPER 250

#define ERROR_NONE 0
#define ERROR_COMMAND 1
#define ERROR_FILE 2

#define COMMAND_NONE 0
#define COMMAND_GAME 1
#define COMMAND_AGENTS 2
#define COMMAND_BASES 3
#define COMMAND_DOALL 4
#define COMMAND_CHEAT_MONEY 5
#define COMMAND_SUPER_AGENT 6
#define COMMAND_BASE_DONE 7
#define COMMAND_BASE_START 8
#define COMMAND_INIT_CHEAT 9

#define DEFAULT_SAVEPATH "./"
#define DEFAULT_SAVEFILE "Savegame/SAVEGAME.00"
#define UFO2PEXE_FILE "Ufoexe/Ufo2p.exe"
#define MAXCHAR_PATHFILE 512

typedef unsigned char  abyte;
typedef unsigned short aword;
typedef unsigned int   adwrd;
typedef struct {
	char name[AGENT_MAXCHAR];
	abyte count;
	abyte unknown01[7];
	abyte rank; /* 0 - rookie, squaddie, squad leader, ?? */
	abyte role; /* 0 - XCom Agent, 1 - biochem, 2 - engineer, 3 - qphysic */
	aword base;
	aword service;
	aword kills;
	aword misions;
	abyte unknown02;
	abyte medals;
	abyte init_maxspeed;
	abyte init_maxhealth;
	abyte init_stamina; /* x / 2 = stamina */
	abyte init_reactions;
	abyte init_strength;
	abyte init_bravery; /* x * 10 = bravery */
	abyte init_accuracy; /* 100 - x = accuracy  */
	abyte init_psi_E;
	abyte init_psi_att;
	abyte init_psi_def;
	abyte unknown03; /* score? */
	abyte curr_role;
	abyte curr_maxspeed;
	abyte curr_maxhealth;
	abyte curr_health;
	abyte unknown04; /* battlescape related? */
	abyte curr_stamina;
	abyte curr_reactions;
	abyte curr_strength;
	abyte curr_bravery; /* max 10 = 100? */
	abyte curr_psi_E;
	abyte curr_psi_att;
	abyte curr_psi_def;
	abyte unknown05;
	abyte curr_accuracy; /* 100 - x = accuracy  */
	abyte unknown06;
	abyte unknown07;
	abyte skill_biochem; /* can be 200! 250? */
	abyte skill_qphysic;
	abyte skill_engineer;
	aword unknown08;
	abyte armor_leg;
	abyte armor_body;
	abyte armor_left;
	abyte armor_right;
	abyte armor_head;
	abyte armor_unknown; /* if activated, cannot put armor on? */
	aword inventoryID; /* FFFF for none */
	abyte unknown09[39];
	abyte assign_type; /* do not edit! */
	aword assign; /* type index */
	aword tgt_dst; /* target (going) or origin (for hire) building */
	abyte unknown10[49];
	abyte gender; /* 0-male, 1-female, 2-android */
	abyte unknown11;
	abyte image; /* 0-26 f, 30-34hyb, 35-64m, 65-69hyb, 70-74a */
	abyte unknown12[18];
	abyte training; /* huh? */
	abyte unknown13[5];
} agent;
typedef struct {
	aword name_index;
	aword xpos1;
	aword xpos2;
	aword ypos1;
	aword ypos2;
	abyte unknown01[160];
	aword function;
	abyte unknown02[10];
	aword purchaseable;
	aword unknown03;
	aword price;
	abyte unknown04[12];
	aword owner;
	abyte unknown05[10];
	abyte alien_counts[14];
} building;
typedef struct {
	aword building_index;
	char name[BASE_MAXCHAR-1];
	abyte unknown01;
	abyte build_type[BASE_TILE_COUNT];
	abyte build_to_finish[BASE_TILE_COUNT];
	/* 150 bytes till this point */
	aword unknown02;
	aword unused_air_guns[5];
	aword alien_disruptors[3];
	aword unused_missiles[5];
	aword alien_missiles[3];
	aword unused_air_def[2];
	aword unused_air_eng[5];
	aword sd_elite; /* req. fusion fuel */
	aword unused_car_def[6];
	aword unused_car_eng[5];
	aword unused_wc[2];
	aword heavy_wc;
	aword alien_wc;
	aword unused_modules[2];
	aword alien_module;
	aword evasion_matrix;
	aword alien_stuff[5];
	/* 248 bytes till this point */
	aword unknown03[41];
	aword disruptor_gun;
	aword devastator_cannon;
	aword unknown04[44];
	aword fusionfuel;
	aword elerium;
	aword items_unknown[138];
} base;
typedef struct {
	int money;
	abyte unknown01[434];
} funding;
typedef struct {
	char label[LABEL_MAXCHAR];
	building city_build[MAXCOUNT_BUILDING];
	base xcom_base[MAXCOUNT_BASE];
	agent xcom_agent[MAXCOUNT_AGENT];
	funding xcom_fund;
} xcom3save;

int finish_base(base *pbase) {
	int loop;

	/* finish all building */
	for (loop=0;loop<BASE_TILE_COUNT;loop++) {
		pbase->build_to_finish[loop] = 0x00;
	}

	return 0;
}

int check_base(base *pbase) {
	int loop, retval = 0;
	abyte *ptile = &pbase->build_type[0];

	for (loop=0;loop<BASE_TILE_COUNT;loop++) {
		/* look for access lift */
		if (ptile[loop]==0x10) {
			retval = 1;
			break;
		}
	}

	return retval;
}

int items_base(base *pbase) {
	/* found alien disruptor weapons */
	pbase->alien_disruptors[0] = 1;
	pbase->alien_disruptors[1] = 1;
	pbase->alien_disruptors[2] = 1;
	pbase->disruptor_gun = 1;
	pbase->devastator_cannon = 1;
	/* the best engine around */
	pbase->sd_elite = 10;
	/* power-up vehicles */
	pbase->evasion_matrix = 10;
	pbase->heavy_wc = 10;
	pbase->alien_wc = 1;

	return 0;
}

int fuel_base(base *pbase) {
	/* req. fusion fuel */
	pbase->fusionfuel = 1000;
	pbase->elerium = 500; /* do i need this? */

	return 0;
}

int complete_bases(xcom3save* psave, FILE* aFile) {
	int loop;

	fseek(aFile,OFFSET_BASE,SEEK_SET); // assume success (0)
	for (loop=0;loop<MAXCOUNT_BASE;loop++) {
		if (!check_base(&psave->xcom_base[loop]))
			break;
		finish_base(&psave->xcom_base[loop]);
		fuel_base(&psave->xcom_base[loop]);
		fwrite((void*)&psave->xcom_base[loop],sizeof(base),1,aFile);
	}
	printf("*CHEAT* Completed all building! (%d)\n", loop);

	return 0;
}

int start_bases(xcom3save* psave, FILE* aFile) {
	int loop;

	fseek(aFile,OFFSET_BASE,SEEK_SET); // assume success (0)
	for (loop=0;loop<MAXCOUNT_BASE;loop++) {
		if (!check_base(&psave->xcom_base[loop]))
			break;
		items_base(&psave->xcom_base[loop]);
		fuel_base(&psave->xcom_base[loop]);
		finish_base(&psave->xcom_base[loop]);
		fwrite((void*)&psave->xcom_base[loop],sizeof(base),1,aFile);
	}
	printf("*CHEAT* Bases powered up! (%d)\n", loop);


	return 0;
}

int print_base(base *pbase, FILE *nFile, int bname_idx) {
	int base_tile = 0;
	int corridors = 0;
	int loop, count = 0;
	int bcount, index, tindex;
	char build_name[256];
	abyte *ptile = &pbase->build_type[0];
	abyte *pdone = &pbase->build_to_finish[0];

	/* count base tiles */
	for (loop=0;loop<BASE_TILE_COUNT;loop++) {
		if (ptile[loop]==0x00)
			base_tile++;
		else if (ptile[loop]>0x00&&ptile[loop]<0x10)
			corridors++;
		if (pdone[loop]>0x00&&ptile[loop]<0x22)
			count++;
	}

	/* get building name */
	fseek(nFile,0x149dd2,SEEK_SET); // assume success (0)
	bcount = 0;
	while (bcount<bname_idx) {
		build_name[0] = (char) fgetc(nFile);
		if (build_name[0] == 0x00)
			bcount++;
	}
	index = 0;
	do {
		build_name[index] = (char) fgetc(nFile);
	} while(build_name[index++]!=0x00);

	printf("----------\n");
	printf("XCom Bases\n");
	printf("----------\n");
	printf("Base Name: %s\n",pbase->name);
	printf("Building Name: %s\n",build_name);
	printf("Buildable tile: %d\n",base_tile);
	printf("Available tile: %d\n",corridors);
	printf("Tile under construction: %d\n",count);

	for (loop=0;loop<BASE_TILE_COUNT;loop++) {
		if (ptile[loop]>0x0F&&ptile[loop]<0x22) {
			/* get tile name */
			fseek(nFile,0x14a67b,SEEK_SET); // assume success (0)
			tindex = ptile[loop] - 0x10;
			bcount = 0;
			while (bcount<tindex) {
				build_name[0] = (char) fgetc(nFile);
				if (build_name[0] == 0x00)
					bcount++;
			}
			index = 0;
			do {
				build_name[index] = (char) fgetc(nFile);
			} while (build_name[index++]!=0x00);

			printf("Tile: %s\n",build_name);
		}
	}

	return 0;
}

int view_bases(xcom3save* psave, FILE* nFile) {
	int loop, count = 0, bname_idx, bindex;

	for (loop=0;loop<MAXCOUNT_BASE;loop++) {
		if (!check_base(&psave->xcom_base[loop]))
			break;
		bindex = psave->xcom_base[loop].building_index;
		bname_idx = psave->city_build[bindex].name_index;
		print_base(&psave->xcom_base[loop],nFile,bname_idx);
		count++;
	}
	printf("XCom base count: %d\n",count);

	return 0;
}

int print_soldier(agent *anagent) {
	printf("Name: %s, ", anagent[0].name);
	printf("Role: %d, ", anagent[0].role);
	printf("Gender: %d, ", anagent[0].gender);
	printf("MaxSpeed: %d, MaxHealth: %d, Health: %d\n",
		anagent[0].curr_maxspeed, anagent[0].curr_maxhealth,
		anagent[0].curr_health);
	printf("Stamina: %d (%d%%), Reactions: %d, Strength: %d, ",
		anagent[0].curr_stamina, anagent[0].curr_stamina/2,
		anagent[0].curr_reactions, anagent[0].curr_strength);
	printf("Bravery: %d (%d%%), Accuracy: %d (%d%%)\n",
		anagent[0].curr_bravery, anagent[0].curr_bravery*10,
		anagent[0].curr_accuracy, (100-anagent[0].curr_accuracy));
	printf("PSI Energy: %d, PSI Attack: %d, PSI Defense: %d\n",
		anagent[0].curr_psi_E, anagent[0].curr_psi_att,
		anagent[0].curr_psi_def);

	return 0;
}

int super_soldier(agent *anagent) {
	if (anagent->init_maxspeed>=AGENT_POWER_SUPER) {
		/* just heal if already made super */
		anagent->curr_health = anagent->curr_maxhealth;
		return 0;
	}

	anagent[0].init_maxspeed = AGENT_POWER_SUPER;
	anagent[0].init_maxhealth = AGENT_POWER_SUPER;
	anagent[0].init_stamina = AGENT_PWRPP_SUPER*2;
	anagent[0].init_reactions = AGENT_POWER_SUPER;
	anagent[0].init_strength = AGENT_POWER_SUPER;
	anagent[0].init_bravery = AGENT_PWRPP_SUPER/10;
	anagent[0].init_accuracy = 100-AGENT_PWRPP_SUPER;
	anagent[0].init_psi_E = AGENT_POWER_SUPER;
	anagent[0].init_psi_att = AGENT_POWER_SUPER;
	anagent[0].init_psi_def = AGENT_POWER_SUPER;
	anagent[0].curr_maxspeed = AGENT_POWER_SUPER;
	anagent[0].curr_maxhealth = AGENT_POWER_SUPER;
	anagent[0].curr_health = AGENT_POWER_SUPER;
	anagent[0].curr_stamina = AGENT_PWRPP_SUPER*2;
	anagent[0].curr_reactions = AGENT_POWER_SUPER;
	anagent[0].curr_strength = AGENT_POWER_SUPER;
	anagent[0].curr_bravery = AGENT_PWRPP_SUPER/10;
	anagent[0].curr_psi_E = AGENT_POWER_SUPER;
	anagent[0].curr_psi_att = AGENT_POWER_SUPER;
	anagent[0].curr_psi_def = AGENT_POWER_SUPER;
	anagent[0].curr_accuracy = 100-AGENT_PWRPP_SUPER;

	return 0;
}

int print_scientist(agent *anagent) {
	printf("Name: %s, ", anagent[0].name);
	printf("Role: %d, ", anagent[0].role);
	printf("Gender: %d\n", anagent[0].gender);
	printf("Biochemist skill: %d, Q.Physicist skill: %d, Engineer skill: %d\n",
		anagent[0].skill_biochem, anagent[0].skill_qphysic,
		anagent[0].skill_engineer);

	return 0;
}

int super_scientist(agent *anagent) {
	anagent[0].skill_biochem = AGENT_SKILL_SUPER;
	anagent[0].skill_qphysic = AGENT_SKILL_SUPER;
	anagent[0].skill_engineer = AGENT_SKILL_SUPER;

	return 0;
}

int super_agents(xcom3save *psave, FILE* aFile) {
	int loop;
	agent *pagent = &psave->xcom_agent[0];

	for (loop=0;loop<MAXCOUNT_AGENT;loop++) {
		/* skip if not counted */
		if (pagent[loop].count==0||pagent[loop].base==0xffff)
			continue;
		/* check if assigned to XCom base */
		if (check_base(&psave->xcom_base[pagent[loop].base])) {
			if (pagent[loop].role == 0)
				super_soldier(&pagent[loop]);
			else if (pagent[loop].role > 0)
				super_scientist(&pagent[loop]);
		}
	}
	fseek(aFile,OFFSET_AGENT,SEEK_SET); // assume success (0)
	for (loop=0;loop<MAXCOUNT_AGENT;loop++) {
		fwrite((void*)&psave->xcom_agent[loop],sizeof(agent),1,aFile);
	}
	printf("*CHEAT* Upgraded to super agents!\n");

	return 0;
}

int view_agents(xcom3save* psave) {
	int loop, count = 0;
	agent *pagent = &psave->xcom_agent[0];

	printf("-----------\n");
	printf("XCom Agents\n");
	printf("-----------\n");
	for (loop=0;loop<MAXCOUNT_AGENT;loop++) {
		/* skip if not counted */
		if (pagent[loop].count==0||pagent[loop].base==0xffff)
			continue;
		/* check if assigned to XCom base */
		if (check_base(&psave->xcom_base[pagent[loop].base])) {
			if (pagent[loop].role == 0) {
				print_soldier(&pagent[loop]);
				count++;
			}
		}
	}

	printf("----------------\n");
	printf("XCom Biochemists\n");
	printf("----------------\n");
	for (loop=0;loop<MAXCOUNT_AGENT;loop++) {
		/* skip if not counted */
		if (pagent[loop].count==0||pagent[loop].base==0xffff)
			continue;
		/* check if assigned to XCom base */
		if (check_base(&psave->xcom_base[pagent[loop].base])) {
			if (pagent[loop].role == 1) {
				print_scientist(&pagent[loop]);
				count++;
			}
		}
	}

	printf("--------------\n");
	printf("XCom Engineers\n");
	printf("--------------\n");
	for (loop=0;loop<MAXCOUNT_AGENT;loop++) {
		/* skip if not counted */
		if (pagent[loop].count==0||pagent[loop].base==0xffff)
			continue;
		/* check if assigned to XCom base */
		if (check_base(&psave->xcom_base[pagent[loop].base])) {
			if (pagent[loop].role == 2) {
				print_scientist(&pagent[loop]);
				count++;
			}
		}
	}

	printf("-----------------------\n");
	printf("XCom Quantum Physicists\n");
	printf("-----------------------\n");
	for (loop=0;loop<MAXCOUNT_AGENT;loop++) {
		/* skip if not counted */
		if (pagent[loop].count==0||pagent[loop].base==0xffff)
			continue;
		/* check if assigned to XCom base */
		if (check_base(&psave->xcom_base[pagent[loop].base])) {
			if (pagent[loop].role == 3) {
				print_scientist(&pagent[loop]);
				count++;
			}
		}
	}

	printf("XCom Personnel count: %d\n",count);

	return 0;
}

int cheat_money(xcom3save* psave, FILE* aFile) {
	psave->xcom_fund.money = FUNDING_MAX;
	fseek(aFile,OFFSET_FUNDING,SEEK_SET); // assume success (0)
	fwrite((void*)&psave->xcom_fund,sizeof(funding),1,aFile);
	printf("*CHEAT* New amount for money: %d\n", psave->xcom_fund.money);

	return 0;
}

int view_game(xcom3save* psave) {
	printf("Game money: %d\n",psave->xcom_fund.money);

	return 0;
}

int read_save(FILE* aFile, xcom3save* psave) {
	int loop;

#ifdef __KDEBUG__
	printf("Size struct for building = %d\n",(int)sizeof(building)); // should be 226
	printf("Size struct for base = %d\n",(int)sizeof(base)); // should be 702
	printf("Size struct for agent = %d\n",(int)sizeof(agent)); // should be 206
#endif

	/* get savegame label */
	fseek(aFile,OFFSET_LABEL,SEEK_SET); // assume success (0)
	fread((void*)psave->label,LABEL_MAXCHAR,1,aFile);

	/* get building data */
	fseek(aFile,OFFSET_BUILDING,SEEK_SET);
	for (loop=0;loop<MAXCOUNT_BUILDING;loop++) {
		fread((void*)&psave->city_build[loop],sizeof(building),1,aFile);
	}

	/* get base data */
	fseek(aFile,OFFSET_BASE,SEEK_SET); // assume success (0)
	for (loop=0;loop<MAXCOUNT_BASE;loop++) {
		fread((void*)&psave->xcom_base[loop],sizeof(base),1,aFile);
	}
	/* check building index for first base ONLY */
	if (psave->city_build[psave->xcom_base[0].building_index].owner
			!= BUILDOWN_XCOM) {
		/* failed to find the building? */
		fprintf(stderr,"Invalid building index for first base!\n");
		return -1;
	}

	/* get data for ALL agent slots */
	fseek(aFile,OFFSET_AGENT,SEEK_SET); // assume success (0)
	for (loop=0;loop<MAXCOUNT_AGENT;loop++) {
		fread((void*)&psave->xcom_agent[loop],sizeof(agent),1,aFile);
	}

	/* get xcom funding */
	fseek(aFile,OFFSET_FUNDING,SEEK_SET); // assume success (0)
	fread((void*)&psave->xcom_fund,sizeof(funding),1,aFile);

	return 0;
}

int main(int argc, char* argv[]) {
	FILE* pFile;
	FILE* eFile;
	xcom3save xsave;
	char fname[] = DEFAULT_SAVEFILE, *pname = fname;
	char fpath[] = DEFAULT_SAVEPATH, *ppath = fpath;
	int arg_cmd = COMMAND_NONE, loop;
	char fullname[MAXCHAR_PATHFILE];

	/** DEBUG **/
#ifdef __KDEBUG__
	printf("Compiler machine: ");
#ifdef __x86_64__
	printf("__x86_64__!\n");
 /* #ifdef __i386__ */
 #else
	printf("__i386__!\n");
#endif
#endif

	if (argc>0) {
		for (loop=1; loop<argc; loop++) {
			if (argv[loop][0]!='-'&&arg_cmd==COMMAND_NONE) /* get a command */ {
				if (strcmp(argv[loop],"game")==0) {
					arg_cmd = COMMAND_GAME;
				}
				else if (strcmp(argv[loop],"agents")==0) {
					arg_cmd = COMMAND_AGENTS;
				}
				else if (strcmp(argv[loop],"bases")==0) {
					arg_cmd = COMMAND_BASES;
				}
				else if (strcmp(argv[loop],"doall")==0) {
					arg_cmd = COMMAND_DOALL;
				}
				else if (strcmp(argv[loop],"more$")==0) {
					arg_cmd = COMMAND_CHEAT_MONEY;
				}
				else if (strcmp(argv[loop],"supers")==0) {
					arg_cmd = COMMAND_SUPER_AGENT;
				}
				else if (strcmp(argv[loop],"build")==0) {
					arg_cmd = COMMAND_BASE_DONE;
				}
				else if (strcmp(argv[loop],"start")==0) {
					arg_cmd = COMMAND_BASE_START;
				}
				else if (strcmp(argv[loop],"init")==0) {
					arg_cmd = COMMAND_INIT_CHEAT;
				}
				else {
					fprintf(stderr,"Unknown command '%s'!\n",argv[loop]);
					return ERROR_COMMAND;
				}
			}
			else if (strcmp(argv[loop],"-f")==0) {
				if (++loop==argc) {
					fprintf(stderr,"No argument for filename!\n");
					return ERROR_FILE;
				}
				pname = argv[loop];
			}
			else if (strcmp(argv[loop],"-p")==0) {
				if (++loop==argc) {
					fprintf(stderr,"No argument for pathname!\n");
					return ERROR_FILE;
				}
				ppath = argv[loop];
			}
			else {
				fprintf(stderr,"Unknown option '%s'!\n",argv[loop]);
				return ERROR_COMMAND;
			}
		}
	}

	/* abort with a warning if no command */
	if (arg_cmd==COMMAND_NONE) {
		fprintf(stdout,"No command found!\n");
		return ERROR_NONE;
	}

	/* savefile name */
	strcpy(fullname,ppath);
	strcat(fullname,pname);

	/* try to open file first */
	pFile = fopen(fullname,"rb+");
	if (!pFile) {
		fprintf(stderr,"Cannot open file '%s'!\n",fullname);
		return ERROR_FILE;
	}

	/* load game data */
	read_save(pFile, &xsave);

	/* ufo2p.exe file */
	strcpy(fullname,ppath);
	strcat(fullname,UFO2PEXE_FILE);

	/* try to open file first */
	eFile = fopen(fullname,"rb");
	if (!eFile) {
		fprintf(stderr,"Cannot open file '%s'!\n",fullname);
		return ERROR_FILE;
	}

	printf("----------------------------------------\n");
	printf("XCom3: Apocalypse - Savegame File Editor\n");
	printf("----------------------------------------\n");
	printf("Game label: %s\n",xsave.label);

	/* execute command */
	switch (arg_cmd) {
		case COMMAND_GAME:
			view_game(&xsave);
			break;
		case COMMAND_AGENTS:
			view_agents(&xsave);
			break;
		case COMMAND_BASES:
			view_bases(&xsave,eFile);
			break;
		case COMMAND_DOALL:
			view_game(&xsave);
			view_agents(&xsave);
			view_bases(&xsave,eFile);
			break;
		case COMMAND_CHEAT_MONEY:
			cheat_money(&xsave,pFile);
			break;
		case COMMAND_SUPER_AGENT:
			super_agents(&xsave,pFile);
			break;
		case COMMAND_BASE_DONE:
			complete_bases(&xsave,pFile);
			break;
		case COMMAND_BASE_START:
			start_bases(&xsave,pFile);
			break;
		case COMMAND_INIT_CHEAT:
			cheat_money(&xsave,pFile);
			super_agents(&xsave,pFile);
			start_bases(&xsave,pFile);
			break;
	}

	fclose(eFile);
	fclose(pFile);

	return 0;
}
